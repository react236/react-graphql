import React, { Component } from 'react'
import { GeistProvider, CssBaseline } from '@geist-ui/core'

import SimpleTabs from './components/SimpleTabs/SimpleTabs'

class App extends Component {
    render() {
        return (
            <GeistProvider>
                <CssBaseline />
                <SimpleTabs />
            </GeistProvider>
        )
    }
}

export default App
