import React, { useState } from "react"
import { Card } from "@geist-ui/core"

import { Register } from './Register/Register'
import { Login } from './Login/Login'

import styles from './styles.module.scss'

export const AuthComponent = () => {
    const [state, setState] = useState(false) // false = login, true = register

    const handleClick = () => {
        setState(prevState => !prevState)
    }

    return (
        <div className={styles.container}>
            <Card hoverable width="300px" type="success" className={styles.card}>
                {state ? <Register /> : <Login />}
                <div className={styles.hint} onClick={handleClick}>{state ? 'Login' : 'Register'}</div>
            </Card>
        </div>
    )
}
