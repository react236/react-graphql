import React, { useState } from "react"
import { useLazyQuery } from "@apollo/client"
import { toast } from "react-hot-toast"
import { useNavigate } from "react-router-dom"
import { Card, Input, Spacer, Button } from "@geist-ui/core"

import useGlobalState from '../../../state'

import { loginQuery } from "../../../queries/queries"

import styles from './styles.module.scss'

export const Login = () => {
    const [login, setLogin] = useState('')
    const [pwd, setPwd] = useState('')
    const [token, setToken] = useGlobalState('token')
    const [globalLogin, setGlobalLogin] = useGlobalState('login')
    const navigate = useNavigate()

    const [doLogin, {loading, error, data}] = useLazyQuery(loginQuery, {
        onCompleted: data => handleCompleted(data),
        fetchPolicy: 'no-cache',
    })

    const handleChange = (name) => {
        return (e) => {
            switch (name) {
                case 'login': {
                    setLogin(e.target.value)
                    return
                }
                case 'pwd': {
                    setPwd(e.target.value)
                    return
                }
                default: {
                    return
                }
            }
        }
    }

    const handleLogin = async (e) => {
        if (!login || !pwd) {
            toast.error('Login and password can\'t be empty', { position: 'bottom-center' })
            return
        }
        const result = await doLogin({
            variables: { login, pwd },
        })
    }

    const handleCompleted = (data) => {
        const token = data.login.token

        if (token === '') {
            toast.error('Incorrect login or password', { position: 'bottom-center' })
            return
        }

        setToken(token)
        setGlobalLogin(login)
        localStorage.setItem('react-graphql-user-token', token)
        navigate('/')

    }

    return (
        <>
            <Card.Content>
                <div className={styles.header}>Login information</div>
            </Card.Content>
            <Card.Content>
                <Input name='login' value={login} placeholder="Login" width="100%" onChange={handleChange('login')} />
                <Spacer />
                <Input name='pwd' value={pwd} placeholder="Password" width="100%" onChange={handleChange('pwd')} />
            </Card.Content>
            <Card.Content className={styles.buttonContainer}>
                <Button auto loading={loading} scale={0.75} onClick={handleLogin}>Login</Button>
            </Card.Content>
        </>
    )
}
