import React, { useEffect } from "react"
import { useNavigate } from "react-router-dom"
import { useApolloClient } from "@apollo/client"
import { Text } from "@geist-ui/core"

import useGlobalState from '../../../state'

import styles from './styles.module.scss'

export const Logout = () => {
    const [token, setToken] = useGlobalState('token')
    const [globalLogin, setGlobalLogin] = useGlobalState('login')
    const navigate = useNavigate()
    const client = useApolloClient()

    useEffect(() => {
        setToken('')
        setGlobalLogin('')
        localStorage.removeItem('react-graphql-user-token')
        client.clearStore()
        navigate('/login')
    })

    return (
        <div className={styles.container}>
            <Text span font="16px">Logging out ...</Text>
        </div>
    )
}
