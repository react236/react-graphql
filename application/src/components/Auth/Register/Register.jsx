import React, { useState } from "react"
import { useMutation } from "@apollo/client"
import { toast } from "react-hot-toast"
import { useNavigate } from "react-router-dom"
import { Card, Input, Spacer, Button } from "@geist-ui/core"

import useGlobalState from '../../../state'

import { addUserMutation } from "../../../queries/mutations"

import styles from './styles.module.scss'

export const Register = () => {
    const [login, setLogin] = useState('')
    const [pwd, setPwd] = useState('')
    const [pwd2, setPwd2] = useState('')
    const [email, setEmail] = useState('')
    const [token, setToken] = useGlobalState('token')
    const [globalLogin, setGlobalLogin] = useGlobalState('login')
    const navigate = useNavigate()

    const [addUser, addUserResult] = useMutation(addUserMutation, {
        onCompleted: data => handleCompleted(data),
        fetchPolicy: 'no-cache',
    })

    const handleChange = (name) => {
        return (e) => {
            switch (name) {
                case 'login': {
                    setLogin(e.target.value)
                    return
                }
                case 'pwd': {
                    setPwd(e.target.value)
                    return
                }
                case 'pwd2': {
                    setPwd2(e.target.value)
                    return
                }
                case 'email': {
                    setEmail(e.target.value)
                    return
                }
                default: {
                    return
                }
            }
        }
    }

    const handleRegister = async (e) => {
        if (!login || !pwd || !pwd2 || !email) {
            toast.error('All fields must be filled', { position: 'bottom-center' })
            return
        }
        if (pwd !== pwd2) {
            toast.error('Passwords must be equal', { position: 'bottom-center' })
            return
        }
        // Регистрация пользователя
        try {
            const result = await addUser({ variables: { login, pwd, email }, })
            if (result.data.addUser === null || result.data.addUser.token === '') {
                toast.error(`Registration error`, { position: 'bottom-center' })
                return
            }
            setToken(result.data.addUser.token)
            setGlobalLogin(login)
            localStorage.setItem('react-graphql-user-token', result.data.addUser.token)
            navigate('/')
        } catch (e) {
            console.log('Error:', e.message)
        }
    }

    const handleCompleted = (data) => {
    }

    return (
        <>
            <Card.Content>
                <div className={styles.header}>Register</div>
            </Card.Content>
            <Card.Content>
                <Input name='login' value={login} placeholder="Login" width="100%" onChange={handleChange('login')} />
                <Spacer />
                <Input name='pwd' value={pwd} placeholder="Password" width="100%" onChange={handleChange('pwd')} />
                <Spacer />
                <Input name='pwd2' value={pwd2} placeholder="Confirm password" width="100%" onChange={handleChange('pwd2')} />
                <Spacer />
                <Input name='email' value={email} placeholder="Email" width="100%" onChange={handleChange('email')} />
                <Spacer />
            </Card.Content>
            <Card.Content className={styles.buttonContainer}>
                <Button auto loading={addUserResult.loading} scale={0.75} onClick={handleRegister}>Register</Button>
            </Card.Content>
        </>
    )
}
