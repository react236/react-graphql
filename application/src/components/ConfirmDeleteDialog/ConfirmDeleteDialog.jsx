import React from 'react'
import { useMutation  } from '@apollo/client'
import { toast } from 'react-hot-toast'

import { Modal, Text } from '@geist-ui/core'
import { XCircle, CheckInCircle } from '@geist-ui/icons'

export const ConfirmDeleteDialog = ({ deleteMutation, dataToDelete, open, handleClose, refetchFunc }) => {
    const [deleteData, { newData, loading, error }] = useMutation(deleteMutation)

    const handleDelete = async () => {
        try {
            await deleteData({
                variables: {
                    id: dataToDelete.id
                },
            })
            await refetchFunc()
            toast.success('Sucess')
        } catch (e) {
            console.log(`Error: ${e.message}`)
        } finally {
            handleClose()
        }
    }

    return (
        <Modal
            visible={open}
            onClose={handleClose}
            aria-labelledby="confirm-dialog"
            aria-describedby="confirm-dialog"
        >
            <Modal.Title>{"Are you sire that you want to delete element?"}</Modal.Title>
            <Modal.Content>
                <Text>If you click 'Confirm' this element will be removed from data base.</Text>
            </Modal.Content>
            <Modal.Action onClick={handleClose} color="primary"><XCircle /> Cancel</Modal.Action>
            <Modal.Action onClick={handleDelete} color="primary"><CheckInCircle /> Confirm</Modal.Action>
        </Modal>
    )
}
