import React, { useEffect, useState, Fragment } from "react"
import { useQuery, useMutation } from '@apollo/client'
import { useSearchParams, useNavigate } from 'react-router-dom'

import { Table } from "@geist-ui/core"
import { ArrowLeft } from "@geist-ui/icons"

import { Img } from "../Img/Img"
import { Loading } from "../Loading/Loading"
import { NewRating } from "../Rating/NewRating"
import { addLeadZero } from "../../utils"

import { directorQuery } from "../../queries/queries"
import { updateRatingMutation } from "../../queries/mutations"
import { IMAGE_SRV } from "../../constants"
import directorPlaceholder from '../../images/director.png'
import moviePlaceholder from '../../images/movie.png'

import styles from './styles.module.scss'

export const DirectorComponent = () => {
    const [searchParams] = useSearchParams()
    const navigate = useNavigate()
    const director = searchParams.get('director')

    useEffect(() => {
        if (!director) {
            navigate('/directors')
        }
    }, [director])

    const { data: directorData, loading: directorLoading, error: directorError } = useQuery(directorQuery, {
        variables: { id: director },
    })
    const [updateRating, updateRatingResult] = useMutation(updateRatingMutation)

    const handleRating = async (id, rate) => {
        await updateRating({
            variables: {
                id,
                rate,
            },
            refetchQueries: [{
                query: directorQuery,
                variables: {
                    id: director,
                },
            }],
        })
    }

    const handleGenreClick = (e) => {
        e.stopPropagation()
        const genreId = e.target.dataset.genreid
        if (genreId) {
            navigate(`/movies?genre=${genreId}`)
        }
    }

    const handleMovieClick = (data) => {
        navigate(`/movie?movie=${data.id}`)
    }

    const getDateFromString = (dateString) => {
        const date = new Date(dateString)

        const [day, month, year] = [
            date.getDate(),
            date.getMonth()+1,
            date.getFullYear(),
        ]

        return `${addLeadZero(day)}.${addLeadZero(month)}.${year}`
    }

    const tableData = !directorData ? [] : directorData.director.movies.map((item) => {
        return {
            id: item.id,
            poster: (
                <Img src={`${IMAGE_SRV}/poster/${item.id}`} placeholder={moviePlaceholder} width="50px" height="66px" />
            ),
            name: item.name,
            year: item.year,
            rate: <NewRating id={item.id} value={item.rate} rateChange={handleRating} />,
            genres: (
                <div onClick={handleGenreClick}>
                    {item.genres.map(item1 => (
                        <Fragment key={item1.id}>
                            <span className={styles.link} data-genreid={item1.id}>{item1.name}</span><br />
                        </Fragment>
                    ))}
                </div>
            ),
        }
    })

    const handleClickBack = (e) => {
        window.history.back()
    }

    if (directorError) return null

    return (
        <div className={styles.container}>
            {directorLoading ? (
                <div className={styles.loading}>
                    <Loading />
                </div>
            ) : (
                <>
                    <div className={styles.arrowBack} onClick={handleClickBack} ><ArrowLeft size={36} /></div>
                    <div className={styles.pictureInfo}>
                        <div className={styles.pictureImage}>
                            <Img
                                src={`${IMAGE_SRV}/director/${directorData.director.id}`}
                                placeholder={directorPlaceholder}
                                alt='Movie poster'
                                className={styles.img}
                            />
                        </div>
                        <div className={styles.info}>
                            <h2 className={styles.name}>{directorData.director.name}</h2>
                            <span>Birthday:&nbsp;&nbsp;{getDateFromString(directorData.director.birthday)}</span>
                        </div>
                    </div>
                    <div className={styles.movies}>
                        <Table data={tableData} onRow={handleMovieClick}>
                            <Table.Column prop="poster" label=""></Table.Column>
                            <Table.Column prop="name" label="Name"></Table.Column>
                            <Table.Column prop="year" label="Year"></Table.Column>
                            <Table.Column prop="rate" label="Rating"></Table.Column>
                            <Table.Column prop="genres" label="Genres"></Table.Column>
                        </Table>
                    </div>
                    <div className={styles.directorMovies}></div>
                </>
            )}
        </div>
    )
}
