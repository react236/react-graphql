import React from 'react'

import DirectorsTable from './DirectorsTable/DirectorsTable'

import styles from './styles.module.scss'

const DirectorsComponent = () => {

    return (
        <>
            <div className={styles.wrapper}>
                <DirectorsTable />
            </div>
        </>
    )

}

export default DirectorsComponent
