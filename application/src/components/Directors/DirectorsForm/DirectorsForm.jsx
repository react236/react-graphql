import React, { useState, useRef } from 'react'
import { useMutation } from '@apollo/client'
import axios from 'axios'

import { Modal, Input, Button, Spacer } from '@geist-ui/core'
import { XCircle, Save } from '@geist-ui/icons'

import { Img } from '../../Img/Img'
import { Loading } from '../../Loading/Loading'

import { addDirectorMutation, updateDirectorMutation } from '../../../queries/mutations'

import { addLeadZero } from '../../../utils'
import { IMAGE_SRV } from '../../../constants'
import directorPlaceholder from '../../../images/director.png'

import styles from './styles.module.scss'

const DirectorsForm = (props) => {
    const pictureRef = useRef(null)
    const pictureBinary = useRef(null)
    const [pictureChanged, setPictureChanged] = useState(false)

    const [addDirector, addDirectorResult] = useMutation(addDirectorMutation)
    const [updateDirector, updateDirectorResult] = useMutation(updateDirectorMutation)

    const {
        open,
        handleEditDialogClose,
        dispatchFunc,
        selectedValue,
        refetchQuery,
    } = props
    const { id, name, birthday } = selectedValue
    const birthdayObject = new Date(birthday)

    // Загрузка фото на сервер
    const pictureUpload = async (url, data) => {
        await axios.post(url, data, {
            headers: {
                'Content-Type': 'application/octet-stream',
            }
        })
    }

    // Обработка фото
    const handlePicture = (e) => {
        // Читаем с диска выбранный файл и ставим его в фото
        const reader = new FileReader()

        reader.onload = (e1) => {
            pictureRef.current.src = e1.target.result
            setPictureChanged(true)

            // Ставим новый обработчик onload
            reader.onload = e2 => {
                // Сохраняем бинарный файл - картинку для отправки
                pictureBinary.current = e2.target.result
            }

            // Читаем файл в бинарном виде для отправки
            reader.readAsArrayBuffer(e.target.files[0])
        }

        reader.readAsDataURL(e.target.files[0])
    }

    // Обработка изменения полей
    const handleChangeField = name => ({ target }) => { 
        switch (name) {
            case ('name'): {
                dispatchFunc({ type: 'setName', payload: target.value })
                break
            }
            case ('birthday'): {
                const birthday = new Date(target.value)
                dispatchFunc({ type: 'setBirthday', payload: birthday })
                break
            }
            default: {
                break
            }
        }
    }

    // Сохранение
    const handleSave = async () => {
        const { id, name, birthday } = selectedValue

        handleEditDialogClose()

        if (id) {
            await updateDirector({
                variables: {
                    id,
                    name,
                    birthday: String(birthday),
                },
            })

            if (id && pictureChanged) {
                await pictureUpload(`${IMAGE_SRV}/director/${id}`, pictureBinary.current)
            }
        } else {
            const newDirector = await addDirector({
                variables: {
                    name,
                    birthday: String(birthday),
                },
            })

            const newId = newDirector.data.addDirector.id

            if (newId && pictureChanged) {
                await pictureUpload(`${IMAGE_SRV}/director/${newId}`, pictureBinary.current)
            }

            await refetchQuery()
        }
    }

    if (addDirectorResult.error || updateDirectorResult.error) return null
    const loadingStatus = addDirectorResult.loading || updateDirectorResult.loading

    return (
        <Modal onClose={handleEditDialogClose} visible={open} aria-labelledby="director-edit-form">
            <Modal.Title className={styles.title} id="director-edit-form">Director information</Modal.Title>
            <Modal.Content>
                <form className={styles.container} noValidate autoComplete="off">
                    {loadingStatus && (
                        <div className={styles.loadingContainer}>
                            <Loading />
                        </div>
                    )}
                    <Input
                        id="name"
                        label="Name"
                        className={styles.textField}
                        value={name}
                        onChange={handleChangeField('name')}
                        width="100%"
                    />
                    <Spacer h={1} />
                    <Input
                        id="birthday"
                        label="Birthday"
                        value={`${birthdayObject.getFullYear()}-${addLeadZero(birthdayObject.getMonth()+1, 2)}-${addLeadZero(birthdayObject.getDate(), 2)}`}
                        onChange={handleChangeField('birthday')}
                        width="100%"
                        htmlType="date"
                    />
                    <Spacer h={1} />
                    <div className={styles.picture}>
                        <Img
                            id="picture" 
                            src={id ? `${IMAGE_SRV}/director/${id}` : ''}
                            placeholder={directorPlaceholder}
                            width="100px" height="133px" className={styles.picture} 
                            ref={pictureRef}
                        />
                        <input type="file"
                            id="pictureInput"
                            name="pictureInput"
                            accept="image/jpeg"
                            onChange={handlePicture}
                        >
                        </input>
                    </div>
                    <div className={styles.wrapper}>
                        <Button onClick={handleSave} className={styles.button}>
                            <Save /> Save
                        </Button>
                        <Button onClick={handleEditDialogClose} className={styles.button}>
                            <XCircle /> Cancel
                        </Button>
                    </div>
                </form>
            </Modal.Content>
        </Modal>
    )
}

export default DirectorsForm
