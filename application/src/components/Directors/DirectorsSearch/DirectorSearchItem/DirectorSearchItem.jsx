import React from 'react'

import { Img } from '../../../Img/Img'

import directorPlaceholder from '../../../../images/director.png'

import styles from './styles.module.scss'

export const DirectorSearchItem = ({ id, directorName, directorImg, onSelect }) => {

    const handleClick = () => {
        onSelect(id)
    }

    return (
        <div className={styles.container} onClick={handleClick}>
            <Img src={directorImg} placeholder={directorPlaceholder} alt='Director picture' width='50px' />
            <div className={styles.descriptionContainer}>
                <div className={styles.directorName}>{directorName}</div>
            </div>
        </div>
    )
}
