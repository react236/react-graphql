import React, { useReducer, Fragment } from 'react'
import { useQuery } from '@apollo/client'
import { useNavigate, useSearchParams } from 'react-router-dom'

import { Table, ButtonDropdown, Pagination } from '@geist-ui/core'
import { XCircle, Edit, MoreHorizontal, Plus, ChevronRight, ChevronLeft } from '@geist-ui/icons'

import DirectorsForm from '../DirectorsForm/DirectorsForm'
import { DirectorsSearch } from '../DirectorsSearch/DirectorsSearch'
import { ConfirmDeleteDialog } from '../../ConfirmDeleteDialog/ConfirmDeleteDialog'
import { Img } from '../../Img/Img'
import { Loading } from '../../Loading/Loading'

import { directorsQuery, directorsPageCountQuery } from '../../../queries/queries'
import { deleteDirectorMutation } from '../../../queries/mutations'

import { addLeadZero, makeSearchParams } from '../../../utils'
import { IMAGE_SRV } from '../../../constants'
import directorPlaceholder from '../../../images/director.png'

import styles from './styles.module.scss'

const initialDirectorData = {
    id: null,
    name: '',
    birthday: String(new Date()),
}

const initialState = {
    firstTimeRender: true,
    openEditDialog: false,
    openDeleteDialog: false,
    directorData: initialDirectorData,
    searchName: '',
}

const reducer = (state, action) => {
    switch (action.type) {
        case 'resetFirstTimeRender': {
            return { ...state, firstTimeRender: false }
        }
        case 'openEditDialog': {
            return { ...state, openEditDialog: true }
        }
        case 'closeEditDialog': {
            return { ...state, openEditDialog: false }
        }
        case 'openDeleteDialog': {
            return { ...state, openDeleteDialog: true }
        }
        case 'closeDeleteDialog': {
            return { ...state, openDeleteDialog: false }
        }
        case 'setDirectorData': {
            return { ...state, directorData: action.payload }
        }
        case 'setSearchName': {
            return { ...state, searchName: action.payload }
        }
        case 'setName': {
            return { ...state, directorData: { ...state.directorData, name: action.payload }}
        }
        case 'setBirthday': {
            return { ...state, directorData: { ...state.directorData, birthday: action.payload }}
        }
        default: {
            throw new Error(`Invalid action '${action.type}'`)
        }
    }
}

export const DirectorsTable = (props) => {
    const [state, dispatch] = useReducer(reducer, initialState)

    const [searchParams] = useSearchParams()
    const navigate = useNavigate()

    const currentPage = searchParams.get('page') ? Number(searchParams.get('page')) : 1

    const { data: directorsPageCountData, loading: directorsPageCountLoading, error: directorsPageCountError } = useQuery(directorsPageCountQuery)
    const { data: directorsData, loading: directorsLoading, error: directorsError, refetch: directorsRefetch } = useQuery(directorsQuery, {
        variables: { page: currentPage-1 },
    })

    const handleDeleteDialogOpen = () => { dispatch({ type: 'openDeleteDialog' }) }
    const handleDeleteDialogClose = () => { dispatch({ type: 'closeDeleteDialog' }) }

    const handleDelete = (directorData) => {
        dispatch({ type: 'setDirectorData', payload: directorData })
        handleDeleteDialogOpen()
    }

    const handleEditDialogOpen = (directorData = initialDirectorData) => {
        dispatch({ type: 'setDirectorData', payload: directorData })
        dispatch({ type: 'openEditDialog' })
    }

    const handleEditDialogClose = (id) => {
        dispatch({ type: 'closeEditDialog' })
        dispatch({ type: 'setDirectorData', payload: initialDirectorData })
        // Перезагружаем изображение (добавляем ненужный параметр в src для обхода кэша)
        const directorImg = document.getElementById(id)
        if (directorImg) {
            const newSrc = `${IMAGE_SRV}/director/${id}?_=${new Date().getTime()}`
            setTimeout(() => {
                directorImg.setAttribute('src', newSrc)
            }, 1000)
        }
    }

    // Переход на другую страницу
    const handlePageChange = async (newPage) => {
        // Обходим глюк (баг) в компоненте Pagination, когда при рендере вызывается handlePageChange(1) при любой начальной странице
        if (state.firstTimeRender) {
            dispatch({ type: 'resetFirstTimeRender' })
            return
        }
        if (currentPage !== newPage) {
            const paramsString = makeSearchParams({
                page: newPage,
            })
            navigate(`/directors${paramsString}`)
        }
    }

    // Клик по строке
    const handleDirectorClick = (data) => {
        navigate(`/director?director=${data.id}`)
    }

    // Клик по фильму
    const handleMovieClick = (e) => {
        e.stopPropagation()
        const movieId = e.target.dataset.movieId
        if (movieId) {
            navigate(`/movie?movie=${movieId}`)
        }
    }

    if (directorsError || directorsPageCountError) {
        return null
    }

    const tableData = !directorsData ? [] : directorsData.directors.map(item => {
        const birthday = new Date(item.birthday)

        return {
            id: item.id,
            picture: (
                <Img src={`${IMAGE_SRV}/director/${item.id}`} placeholder={directorPlaceholder} width="100px" height="133px" id={item.id} />
            ),
            name: item.name,
            birthday: `${addLeadZero(birthday.getDate(), 2)}.${addLeadZero(birthday.getMonth()+1, 2)}.${birthday.getFullYear()}`,
            movies: (
                <div onClick={handleMovieClick}>
                    {item.movies.map(item1 => (
                        <Fragment key={item1.name}><span className={styles.link} data-movieid={item1.id}>{item1.name}</span><br /></Fragment>
                    ))}
                </div>
            ),
            actions: (
                <ButtonDropdown auto icon={<MoreHorizontal />} >
                    <ButtonDropdown.Item main onClick={() => handleEditDialogOpen(item)} className={styles.menuItem}>
                        <Edit />
                        <div>Edit</div>
                    </ButtonDropdown.Item>
                    <ButtonDropdown.Item onClick={() => handleDelete(item)} className={styles.menuItem}>
                        <XCircle />
                        <div>Delete</div>
                    </ButtonDropdown.Item>
                </ButtonDropdown>
            )
        }
    })

    return (
        <>
            {(directorsLoading || directorsPageCountLoading) && (
                <div className={styles.spinner}>
                    <Loading />
                </div>
            )}
            <DirectorsForm
                selectedValue={state.directorData} 
                open={state.openEditDialog}
                dispatchFunc={dispatch}
                handleEditDialogClose={handleEditDialogClose}
                refetchQuery={directorsRefetch} 
            />
            <ConfirmDeleteDialog
                open={state.openDeleteDialog}
                handleClose={handleDeleteDialogClose}
                dataToDelete={state.directorData}
                deleteMutation={deleteDirectorMutation}
                refetchFunc={directorsRefetch}
            />
            <div className={styles.tableContainer}>
                <div className={styles.searchRoot}>
                    <DirectorsSearch />
                </div>
                <Table data={tableData} onRow={handleDirectorClick}>
                    <Table.Column prop="picture" label="Picture" />
                    <Table.Column prop="name" label="Name" />
                    <Table.Column prop="birthday" label="Birthday" />
                    <Table.Column prop="movies" label="Movies" />
                    <Table.Column prop="actions" label="" />
                </Table>
            </div>
            <div className={styles.pagination}>
                {directorsPageCountData && (
                    <Pagination count={directorsPageCountData.directorsPageCount} onChange={handlePageChange} page={currentPage}>
                        <Pagination.Next><ChevronRight /></Pagination.Next>
                        <Pagination.Previous><ChevronLeft /></Pagination.Previous>
                    </Pagination>
                )}
            </div>
            <div onClick={() => handleEditDialogOpen(state.directorData)} color="primary" aria-label="Add" className={styles.fab}>
                <Plus />
            </div>
        </>
    )
}

export default DirectorsTable
