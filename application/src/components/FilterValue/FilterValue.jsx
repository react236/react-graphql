import React from "react"

import { Text } from "@geist-ui/core"
import { X } from '@geist-ui/icons'

import styles from './styles.module.scss'

export const FilterValue = (props) => {
    return (
        <div className={styles.container}>
            <Text span type="secondary">{props.data}&nbsp;&nbsp;</Text>
            <X className={styles.closeHover} color="#666666" onClick={() => props.closeAction(props.dataId)} />
        </div>
    )
}
