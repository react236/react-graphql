import React, { useState } from 'react'

import { Plus } from '@geist-ui/icons'

import GenresTable from './GenresTable/GenresTable'
import GenresForm from './GenresForm/GenresForm'

import styles from './styles.module.scss'

const GenresComponent = (props) => {
    const [open, setOpen] = useState(false)
    const [name, setName] = useState('')
    const [age, setAge] = useState(0)
    const [id, setId] = useState(null)

    const handleClickOpen = (data = {}) => {
        setOpen(true)
        if (data) {
            setName(data.name)
            setAge(data.age)
            setId(data.id)
        }
    }

    const handleClose = () => {
        setName('')
        setAge(0)
        setId(null)
        setOpen(false)
    }

    const handleChange = name => ({ target }) => { 
        switch (name) {
            case ('name'): {
                setName(target.value)
                break
            }
            case ('age'): {
                setAge(target.value)
                break
            }
            default: {
                break
            }
        }
    }

    return (
        <>
            <GenresForm
                handleChange={handleChange}
                selectedValue={{ name, age, id }}
                open={open}
                onClose={handleClose}
            />
            <div className={styles.wrapper}>
                <GenresTable onOpen={handleClickOpen} onClose={handleClose} />
                <div onClick={() => handleClickOpen(null)} color="primary" aria-label="Add" className={styles.fab}>
                    <Plus />
                </div>
            </div>
        </>
    )

}

export default GenresComponent
