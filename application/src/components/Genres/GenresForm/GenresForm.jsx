import React from 'react'
import { useMutation } from '@apollo/client'

import { Modal, Input, Button } from '@geist-ui/core'
import { XCircle, Save } from '@geist-ui/icons'

import { addGenreMutation, updateGenreMutation } from '../../../queries/mutations'
import { genresQuery } from '../../../queries/queries'

import styles from './styles.module.scss'

const GenresForm = (props) => {
    const [addGenre, addGenreResult] = useMutation(addGenreMutation)
    const [updateGenre, updateGenreResult] = useMutation(updateGenreMutation)

    const handleClose = () => {
        props.onClose()
    }

    const handleSave = () => {
        const { selectedValue, onClose } = props
        const { id, name } = selectedValue
        if (id) {
            updateGenre({
                variables: {
                    id,
                    name,
                },
                refetchQueries: [{
                    query: genresQuery,
                }]
            })
        } else {
            addGenre({
                variables: {
                    name,
                },
                refetchQueries: [{
                    query: genresQuery,
                }],
            })
        }
        onClose()
    }

    const { open, handleChange, selectedValue = {} } = props
    const { name } = selectedValue;

    if (addGenreResult.loading || updateGenreResult.loading) return 'Submitting...'
    if (addGenreResult.error || updateGenreResult.error) return `Submission error!`

    return (
        <Modal onClose={handleClose} visible={open} aria-labelledby="genre-edit-form">
            <Modal.Title className={styles.title} id="genre-edit-title">Genre information</Modal.Title>
            <Modal.Content>
                <form className={styles.container} noValidate autoComplete="off">
                    <Input
                        id="name"
                        label="Name"
                        className={styles.textField}
                        value={name}
                        onChange={handleChange('name')}
                        width="100%"
                    />
                    <div className={styles.wrapper}>
                        <Button onClick={handleSave} className={styles.button}>
                            <Save /> Save
                        </Button>
                        <Button onClick={handleClose} className={styles.button}>
                            <XCircle /> Cancel
                        </Button>
                    </div>
                </form>
            </Modal.Content>
        </Modal>
    )
}


export default GenresForm
