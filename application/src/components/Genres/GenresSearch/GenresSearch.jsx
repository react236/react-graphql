import React from 'react'

import { Input } from '@geist-ui/core'
import { Search } from '@geist-ui/icons'

import styles from './styles.module.scss'

const GenresSearch = (props) => {

    const { name, handleSearch, handleChange } = props

    return (
        <div className={styles.search}>
            <div className={styles.searchIcon}>
                <Search />
            </div>
            <Input
                onChange={handleChange}
                onKeyDown={handleSearch}
                value={name}
                placeholder="Search…"
            />
        </div>
    )
}

export default GenresSearch
