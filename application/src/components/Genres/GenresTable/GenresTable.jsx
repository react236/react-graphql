import React, { useReducer, useState } from 'react'
import { useQuery } from '@apollo/client'
import { Link } from 'react-router-dom'

import { Table, ButtonDropdown } from '@geist-ui/core'
import { XCircle, Edit, MoreHorizontal } from '@geist-ui/icons'

import GenresSearch from '../GenresSearch/GenresSearch'
import { ConfirmDeleteDialog } from '../../ConfirmDeleteDialog/ConfirmDeleteDialog'
import { Loading } from '../../Loading/Loading'

import { genresQuery } from '../../../queries/queries'
import { deleteGenreMutation } from '../../../queries/mutations'

import styles from './styles.module.scss'

const initialState = {
    openDialog: false,
    genreData: null,
    searchName: '',
}

const reducer = (state, action) => {
    switch (action.type) {
        case 'openDialog': {
            return { ...state, openDialog: true }
        }
        case 'closeDialog': {
            return { ...state, closeDialog: false }
        }
        case 'setGenreData': {
            return { ...state, genreData: action.payload }
        }
        case 'setSearchName': {
            return { ...state, searchName: action.payload }
        }
        default: {
            throw new Error(`Invalid action '${action.type}'`)
        }
    }
}

export const GenresTable = (props) => {
    const [state, dispatch] = useReducer(reducer, initialState)

    const { data: genresData, loading: genresLoading, error: genresError, refetch: genresRefetch } = useQuery(genresQuery)
    const { loading: genresSearchLoading, data: genresSearchData, fetchMore} = useQuery(genresQuery)

    const handleDialogOpen = () => { dispatch({ type: 'openDialog' }) }
    const handleDialogClose = () => { dispatch({ type: 'closeDialog' }) }

    const handleClose = () => {}

    const handleEdit = (genreData) => {
        dispatch({ type: 'setGenreData', payload: genreData })
        props.onOpen(genreData) // Открываем модальное окно
        handleClose()
    }

    const handleDelete = (genreData) => {
        dispatch({ type: 'setGenreData', payload: genreData })
        handleDialogOpen()
        handleClose()
    }

    const handleSearchChange = (e) => {
        dispatch({ type: 'setSearchName', payload: e.target.value })
    }

    const handleSearch = (e) => {
        if (e.key === 'Enter') {
            fetchMore({
                variables: { name: state.searchName },
                updateQuery: (previousResult, { fetchMoreResult }) => fetchMoreResult
            })
        }
    }

    if (genresError) {
        return null
    }

    const tableData = !genresData ? [] : genresData.genres.map((item) => {
        return {
            id: item.id,
            name: item.name,
            movies: '',
            actions: (
                <ButtonDropdown auto icon={<MoreHorizontal />} >
                    <ButtonDropdown.Item main onClick={() => handleEdit(item)} className={styles.menuItem}>
                        <Edit />
                        <div>Edit</div>
                    </ButtonDropdown.Item>
                    <ButtonDropdown.Item onClick={() => handleDelete(item)} className={styles.menuItem}>
                        <XCircle />
                        <div>Delete</div>
                    </ButtonDropdown.Item>
                </ButtonDropdown>
            )
        }
    })

    const renderShowMovieButton = (value, rowData, rowIndex) => {
        return (
            <Link to={`/movies?genre=${rowData.id}`}>Show Movies</Link>
        )
    }

    return (
        <>
            {(genresLoading) && (
                <div className={styles.spinner}>
                    <Loading />
                </div>
            )}
            <ConfirmDeleteDialog
                open={state.openDialog}
                handleClose={handleDialogClose}
                dataToDelete={state.genreData}
                deleteMutation={deleteGenreMutation}
                refetchFunc={genresRefetch}
            />
            <div className={styles.tableContainer}>
                <div className={styles.searchRoot}>
                    <GenresSearch name={state.searchName} handleChange={handleSearchChange} handleSearch={handleSearch} />
                </div>
                <Table data={tableData}>
                    <Table.Column prop="name" label="Name" />
                    <Table.Column prop="movies" label="Movies" render={renderShowMovieButton} />
                    <Table.Column prop="actions" label="" />
                </Table>
            </div>
        </>
    )

}

export default GenresTable
