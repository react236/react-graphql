import React, { useState, useEffect, forwardRef } from "react"

import cat from './cat.png'

/**
 * @param src - image src
 * @param placeholder - placeholder image
 * @param alt - image alt
 * @param width - image width
 * @param height - image height
 * @param className - image className 
 * @param id - image id
 * @returns React.Component
 */
export const Img = forwardRef( ({ src, placeholder, id, alt="", width, height, className }, ref ) => {
    const [imageReady, setImageReady] = useState(false)

    useEffect(() => {
        const image = new Image()

        image.src = src

        image.onload = () => {
            setImageReady(true)
        }
    })

    let imgSrc = src

    if (!imageReady) {
        imgSrc = placeholder ? placeholder : cat
    }

    return (
        <img
            src={imgSrc}
            alt={alt}
            width={width}
            height={height}
            className={className}
            id={id}
            ref={ref}
        />
    )
})
