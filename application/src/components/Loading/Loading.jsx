import React from "react"
import { SpinnerCircularFixed } from 'spinners-react'

export const Loading = ({ 
    size=60,
    thickness=100,
    speed=100,
    color="rgba(63, 147, 246, 1)",
    secondaryColor="rgba(125, 125, 125, 0.44)",
    show=true
}) => {
    return (
        <>
            {show ? (
                <SpinnerCircularFixed size={size} thickness={thickness} speed={speed} color={color} secondaryColor={secondaryColor} />
            ) : null}
        </>
    )
}
