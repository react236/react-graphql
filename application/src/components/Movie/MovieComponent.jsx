import React, { useEffect, useState } from "react"
import { useQuery, useMutation } from '@apollo/client'
import { useSearchParams, useNavigate } from 'react-router-dom'

import { PlayCircle, ArrowLeft } from "@geist-ui/icons"

import { Img } from "../Img/Img"
import { NewRating as Rating } from '../Rating/NewRating'
import { Loading } from "../Loading/Loading"
import { WatchMovie } from './WatchMovie/WatchMovie'

import { movieQuery } from "../../queries/queries"
import { updateRatingMutation } from "../../queries/mutations"
import { IMAGE_SRV } from "../../constants"
import moviePlaceholder from '../../images/movie.png'

import styles from './styles.module.scss'

export const MovieComponent = () => {
    const [playVideo, setPlayVideo] = useState(false)

    const [searchParams] = useSearchParams()
    const navigate = useNavigate()
    const movie = searchParams.get('movie')

    useEffect(() => {
        if (!movie) {
            navigate('/movies')
        }
    }, [movie])

    const { data: movieData, loading: movieLoading, error: movieError } = useQuery(movieQuery, {
        variables: { id: movie },
    })
    const [updateRating, updateRatingResult] = useMutation(updateRatingMutation)

    const handleRating = async (id, rate) => {
        await updateRating({
            variables: {
                id,
                rate,
            },
            refetchQueries: [{
                query: movieQuery,
                variables: {
                    id: movie,
                },
            }],
        })
    }

    const handleGenreClick = (e) => {
        const genreId = e.target.dataset.genreid
        if (genreId) {
            navigate(`/movies?genre=${genreId}`)
        }
    }

    const handlePlay = (e) => {
        setPlayVideo(true)
    }

    const handleWatchClose = (e) => {
        setPlayVideo(false)
    }

    const handleDirectorClick = (e) => {
        const directorId = e.target.dataset.directorid
        if (directorId) {
            navigate(`/director?director=${directorId}`)
        }
    }

    const handleClickBack = (e) => {
        window.history.back()
    }

    if (movieError) return 'Error...'

    const genresList = !movieData ? null : (
        <div onClick={handleGenreClick}>
            {movieData.movie.genres.map(item => <span key={item.id} className={styles.link} data-genreid={item.id}>{item.name}&nbsp;&nbsp;</span>)}
        </div>
    )

    return (
        <div className={styles.container}>
            {movieLoading ? (
                <div className={styles.loading}>
                    <Loading />
                </div>
            ) : (
                <>
                    <div className={styles.arrowBack} onClick={handleClickBack} ><ArrowLeft size={36} /></div>
                    <div className={styles.posterInfo}>
                        <div className={styles.posterImage} onClick={handlePlay}>
                            <Img
                                src={`${IMAGE_SRV}/poster/${movieData.movie.id}`}
                                placeholder={moviePlaceholder}
                                alt='Movie poster'
                                className={styles.img}
                            />
                            <PlayCircle className={styles.play} />
                        </div>
                        <div className={styles.info}>
                            <h2 className={styles.name}>{movieData.movie.name}</h2>
                            <span>
                                Director:&nbsp;&nbsp;
                                <span
                                    className={styles.link}
                                    onClick={handleDirectorClick}
                                    data-directorid={movieData.movie.director.id}
                                >
                                    {movieData.movie.director.name}
                                </span>
                            </span>
                            <span>Genres:&nbsp;&nbsp;{genresList}</span>
                            <div className={styles.rating}>Rating:&nbsp;&nbsp;<Rating id={movieData.movie.id} value={movieData.movie.rate} rateChange={handleRating} /></div>
                        </div>
                    </div>
                    <div className={styles.description}>{movieData.movie.description}</div>
                </>
            )}
            {playVideo && <WatchMovie id={movieData.movie.id} onClose={handleWatchClose} />}
        </div>
    )
}
