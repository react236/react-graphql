import React from 'react'

import { X } from '@geist-ui/icons'

import { IMAGE_SRV } from '../../../constants'

import styles from './styles.module.scss'

export const WatchMovie = ({ id, onClose }) => {
    return (
        <>
            <div className={styles.back}></div>
            <div className={styles.close} onClick={onClose}>
                <X color="white" size={40} />
            </div>
            <video controls preload="metadata" className={styles.video}>
                <source src={`${IMAGE_SRV}/movie/${id}`} type='video/mp4' />
            </video>
        </>
    )
}
