import React from 'react'

import { Img } from '../../../Img/Img'

import directorPlaceholder from '../../../../images/director.png'

import styles from './styles.module.scss'

export const DirectorForMovieSearchItem = ({ director, directorImg, onSelect }) => {

    const handleClick = (e) => {
        onSelect(director.id)
    }

    return (
        <div className={styles.container} onClick={handleClick}>
            <Img src={directorImg} placeholder={directorPlaceholder} alt='Director picture' width='30px' />
            <div className={styles.directorName}>{director.name}</div>
        </div>
    )
}
