import React, { useState } from 'react'
import { useLazyQuery } from '@apollo/client'

import { Input } from '@geist-ui/core'

import { DirectorForMovieSearchItem } from './DirectorForMovieSearchItem/DirectorForMovieSearchItem'
import { Loading } from '../../Loading/Loading'

import { directorsForAddMovieQuery } from '../../../queries/queries'
import { IMAGE_SRV } from '../../../constants'

import styles from './styles.module.scss'

export const DirectorForMoviesSearch = ({ initialValue, onChange }) => {
    const [searchValue, setSearchValue] = useState(initialValue.name)
    const [showResults, setShowResults] = useState(false)
    const [searchResults, setSearchResults] = useState(null)

    const [getDirectors, { data, loading, error }] = useLazyQuery(directorsForAddMovieQuery, {
        variables: { name: searchValue }
    })

    const handleChange = async (e) => {
        setSearchValue(e.target.value)

        if (e.target.value.length === 0) {
            setShowResults(false)
        } else {
            setShowResults(true)
            const newData = await getDirectors()
            setSearchResults({ ...newData.data })
        }
    }

    const handleSearch = (e) => {

    }

    const handleSelect = (directorId) => {
        setShowResults(false)
        const selectedDirector = searchResults.directors.find(item => item.id === directorId)
        setSearchValue(selectedDirector.name);
        onChange(selectedDirector.id)
    }

    return (
        <div className={styles.container}>
            <Input
                onChange={handleChange}
                onKeyDown={handleSearch}
                name='name'
                value={searchValue}
                placeholder="Search..."
                width="100%"
            />
            {showResults && (
                <div className={styles.searchResults}>
                    {loading && (
                        <div className={styles.spinner}>
                            <Loading size={20} thickness={100} />
                        </div>
                    )}
                    {error ? (
                        <div>Search error!</div>
                    ) : (
                        <>
                            {searchResults && (
                                <>
                                    {searchResults.directors.length > 0 ? (
                                        searchResults.directors.map(item => (
                                            <div className={styles.searchResultsItemWrapper}>
                                                <DirectorForMovieSearchItem
                                                    key={item.id}
                                                    director={item}
                                                    directorImg={`${IMAGE_SRV}/director/${item.id}`}
                                                    onSelect={handleSelect}
                                                />
                                            </div>
                                        ))
                                    ) : (
                                        <div>No directors found</div>
                                    )}
                                </>
                            )}
                        </>
                    )}
                </div>
            )}
        </div>
    )
}
