import React from 'react'

import MoviesTable from './MoviesTable/MoviesTable'

import styles from './styles.module.scss'

const MoviesComponent = (props) => {

    return (
        <div className={styles.wrapper}>
            <MoviesTable />
        </div>
    )

}

export default MoviesComponent
