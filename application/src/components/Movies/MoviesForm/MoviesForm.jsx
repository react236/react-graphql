import React, { useRef, useState } from 'react'
import { useMutation, useQuery } from '@apollo/client'
import axios from 'axios'
import { Circle } from 'rc-progress'

import { Modal, Input, Select, Checkbox, Button, Spacer, Text, Textarea } from '@geist-ui/core'
import { XCircle, Save } from '@geist-ui/icons'

import useChunkedUploader from '../../../utils/chunkUpload'
import { Img } from '../../Img/Img'
import { DirectorForMoviesSearch } from '../DirectorForMovieSearch/DirectorForMoviesSearch'
import { Loading } from '../../Loading/Loading'

import { addMovieMutation, updateMovieMutation } from '../../../queries/mutations'
import { directorsForAddMovieQuery } from '../../../queries/queries'
import { IMAGE_SRV } from '../../../constants'

import moviePlaceholder from '../../../images/movie.png'

import styles from './styles.module.scss'

export const MoviesForm = (props) => {
    const [posterChanged, setPosterChanged] = useState(false)
    const [videoUploading, setVideoUploading] = useState(false)
    const posterRef = useRef(null)
    const posterBinary = useRef(null)
    const videoRef = useRef(null)
    const videoInput = useRef(null)

    const { data: directorsData, loading: directorsLoading, error: directorsError } = useQuery(directorsForAddMovieQuery)
    const [addMovie, addMovieResult] = useMutation(addMovieMutation)
    const [updateMovie, updateMovieResult] = useMutation(updateMovieMutation)

    const { uploadFile, cancelUpload, isLoading, progress } = useChunkedUploader()

    const videoUpload = async () => {
        const url = `${IMAGE_SRV}/movie/upload/${props.selectedValue.id}`
        const chunkSize = 1048576
        const file = videoInput.current.files[0]

        if (!file) return

        setVideoUploading(true)
        try {
            const result = await uploadFile({ file, url, chunkSize })
            return result
        } catch (error) {
            console.error(error)
        } finally {
            setVideoUploading(false)
        }
    }

    const posterUpload = async (url, data) => {
        await axios.post(url, data, {
            headers: {
                'Content-Type': 'application/octet-stream',
            }
        })
    }

    const {
        open,
        handleEditDialogClose,
        genresData,
        dispatchFunc,
        selectedValue = {},
        refetchQuery,
    } = props
    const { id, name, genres, rate, director, watched, year, description } = selectedValue;
    const genresInitialValue = genres ? genres.map(item => item.id) : []

    const handleChangeField = name => ({ target }) => { 
        switch (name) {
            case ('name'): {
                dispatchFunc({ type: 'setName', payload: target.value })
                break
            }
            case ('rate'): {
                dispatchFunc({ type: 'setRate', payload: target.value })
                break
            }
            case ('year'): {
                dispatchFunc({ type: 'setYear', payload: target.value })
                break
            }
            default: {
                break
            }
        }
    }

    // Выбор режиссера
    const handleDirectorChange = ( data ) => {
        const director = directorsData.directors.filter(item => item.id === data)
        dispatchFunc({ type: 'setDirector', payload: { id: director[0].id, name: director[0].name } }) 
    }

    // Выбор жанров
    const handleSelectGenres = ( data ) => {
        dispatchFunc({ type: 'setGenres', payload: data })
    }

    // Описание
    const handleDescriptionChange = ( e ) => {
        dispatchFunc({ type: 'setDescription', payload: e.target.value })
    }

    // Постер
    const handlePoster = (e) => {
        // Читаем с диска выбранный файл и ставим его в постер
        const reader = new FileReader()

        reader.onload = (e1) => {
            posterRef.current.src = e1.target.result
            setPosterChanged(true)

            // Ставим новый обработчик onload
            reader.onload = e2 => {
                // Сохраняем бинарный файл - картинку для отправки
                posterBinary.current = e2.target.result
            }

            // Читаем файл в бинарном виде для отправки
            reader.readAsArrayBuffer(e.target.files[0])
        }

        reader.readAsDataURL(e.target.files[0])
    }

    // Видео
    const handleVideo = (e) => {
        console.log(e.target.files);
    }

    // Из массива id жанров делаем массив объектов жанров
    const transformGenres = (data) => {
        if (Array.isArray(data)) {
            const selectedGenres = data.map( item => genresData.genres.find(item1 => item1.id === item) )
            handleSelectGenres(selectedGenres)
        } else {
            handleSelectGenres([])
        }
    }

    // Сохранение
    const handleSave = async () => {
        const { id, name, genres, watched, rate, director, year, description } = selectedValue
        const genresList = genres.map(item => item.id)

        await videoUpload()

        handleEditDialogClose(id)

        if (id) {
            if (id && posterChanged) {
                await posterUpload(`${IMAGE_SRV}/poster/${id}`, posterBinary.current)
            }

            await updateMovie({
                variables: {
                    id, name, genres: genresList, watched: Boolean(watched), rate: Number(rate), year: Number(year), directorId: director.id, description,
                },
            })
        } else {
            const newMovie = await addMovie({
                variables: {
                    name, genres: genresList, watched: Boolean(watched), rate: Number(rate), year: Number(year), directorId: director.id, description,
                },
            })

            const newId = newMovie.data.addMovie.id

            if (newMovie.data.addMovie.id && posterChanged) {
                await posterUpload(`${IMAGE_SRV}/poster/${newId}`, posterBinary.current)
            }

            await refetchQuery()
        }
    }

    if (directorsError || addMovieResult.error || updateMovieResult.error) return null

    const loadingStatus = directorsLoading || addMovieResult.loading || updateMovieResult.loading

    return (
        <Modal onClose={handleEditDialogClose} visible={open} aria-labelledby="movie-edit-form" width="35%">
            <Modal.Title className={styles.title} id="movie-edit-form">Movie information</Modal.Title>
            <Modal.Content>
                <form className={styles.container} noValidate autoComplete="off">
                    {loadingStatus && (
                        <div className={styles.loadingContainer}>
                            <Loading />
                        </div>
                    )}
                    <div>
                        <Input
                            id="name"
                            label="Name"
                            value={name}
                            onChange={handleChangeField('name')}
                            width="100%"
                        />
                        <Spacer h={1} />
                        <Input
                            id="year"
                            label="Year"
                            value={year}
                            onChange={handleChangeField('year')}
                            width="100%"
                        />
                        <Spacer h={1} />
                    </div>
                    <div>
                        <Text span font="14px">Genres</Text>
                        <Select
                            multiple
                            clearable
                            id="select-genres"
                            onChange={transformGenres}
                            initialValue={genresInitialValue}
                            width="100%"
                        >
                            {genresData.genres.map((item) => (
                                <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                            ))}
                        </Select>
                        <Spacer h={1} />
                        <Input
                            id="rate"
                            label="Rate"
                            value={rate}
                            onChange={handleChangeField('rate')}
                            width="100%"
                        />
                        <Spacer h={1} />
                    </div>
                    <Text span font="14px">Director</Text>
                    <DirectorForMoviesSearch
                        initialValue={director ? director : {}}
                        onChange={handleDirectorChange}
                    />
                    <Spacer h={1} />
                    <Text span font="14px">Description</Text><br />
                    <Textarea
                        value={description}
                        onChange={handleDescriptionChange}
                        placeholder="Please enter a description."
                        width="100%"
                        style={{ height: "100px" }}
                    />
                    <Spacer h={1} />
                    {/* <Checkbox checked={watched} onChange={handleCheckboxChange('watched')} value="watched">Watched movie</Checkbox> */}
                    <div className={styles.uploads}>
                        <div className={styles.poster}>
                            <span className={styles.posterVideoHead}>Poster</span>
                            <Img 
                                id="poster" 
                                src={id ? `${IMAGE_SRV}/poster/${id}` : ''}
                                placeholder={moviePlaceholder}
                                width="100px" height="133px"
                                ref={posterRef}
                            />
                            <input type="file"
                                id="posterInput"
                                name="posterInput"
                                accept="image/jpeg"
                                onChange={handlePoster}
                                style={{ display: 'block' }}
                            >
                            </input>
                        </div>
                        <div className={styles.video} id='videoUpload'>
                            <span className={styles.posterVideoHead}>Video</span>
                            <Img 
                                id="video" 
                                src={''}
                                placeholder={moviePlaceholder}
                                width="100px" height="133px"
                                ref={videoRef}
                            />
                            <input type="file"
                                id="videoInput"
                                name="videoInput"
                                accept="video/mp4, image/*"
                                onChange={handleVideo}
                                style={{ display: 'block' }}
                                ref={videoInput}
                            >
                            </input>
                            {videoUploading && (
                                <Circle className={styles.videoProgress} strokeWidth={3} percent={progress*100} />
                            )}
                        </div>
                    </div>
                    <div className={styles.wrapper}>
                        <Button onClick={handleSave} className={styles.actionButton}>
                            {!videoUploading ? (
                                <>
                                    <Save /> Save
                                </>
                            ) : (
                                <Loading size={20} />
                            )}
                        </Button>
                        <Button onClick={()=>handleEditDialogClose(id)} className={styles.actionButton}>
                            <XCircle /> Cancel
                        </Button>
                    </div>
                </form>
            </Modal.Content>
        </Modal>
    )
}
