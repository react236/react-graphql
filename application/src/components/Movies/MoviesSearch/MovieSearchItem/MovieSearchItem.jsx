import React from 'react'

import { Img } from '../../../Img/Img'

import moviePlaceholder from '../../../../images/movie.png'
import directorPlaceholder from '../../../../images/director.png'

import styles from './styles.module.scss'

export const MoviesSearchItem = ({ id, movieName, directorName, movieImg, directorImg, onSelect }) => {

    const handleClick = () => {
        onSelect(id)
    }

    return (
        <div className={styles.container} onClick={handleClick}>
            <Img src={movieImg} placeholder={moviePlaceholder} alt='Movie poster' width='50px' />
            <div className={styles.descriptionContainer}>
                <div className={styles.movieName}>{movieName}</div>
                <div className={styles.directorContainer}>
                    <Img src={directorImg} placeholder={directorPlaceholder} alt='Director picture' width='20px' />
                    <div className={styles.directorName}>{directorName}</div>
                </div>
            </div>
        </div>
    )
}
