import React, { useState } from 'react'
import { useLazyQuery } from '@apollo/client'
import { useNavigate } from 'react-router-dom'

import { Input } from '@geist-ui/core'
import { Search } from '@geist-ui/icons'

import { MoviesSearchItem } from './MovieSearchItem/MovieSearchItem'
import { Loading } from '../../Loading/Loading'

import { moviesSearchQuery } from '../../../queries/queries'
import { IMAGE_SRV } from '../../../constants'

import styles from './styles.module.scss'

export const MoviesSearch = (props) => {
    const [searchValue, setSearchValue] = useState('')
    const [showResults, setShowResults] = useState(false)
    const [searchResults, setSearchResults] = useState(null)

    const navigate = useNavigate()

    const [getMovies, { data, loading, error }] = useLazyQuery(moviesSearchQuery, {
        variables: { name: searchValue }
    })

    const handleChange = async (e) => {
        setSearchValue(e.target.value)

        if (e.target.value.length === 0) {
            setShowResults(false)
        } else {
            setShowResults(true)
            const newData = await getMovies()
            setSearchResults({ ...newData.data })
        }
    }

    const handleSearch = (e) => {

    }

    const handleSelect = (id) => {
        setShowResults(false)
        navigate(`/movie?movie=${id}`)
    }

    return (
        <>
            <div className={styles.search}>
                <div className={styles.searchIcon}>
                    <Search />
                </div>
                <Input
                    onChange={handleChange}
                    onKeyDown={handleSearch}
                    name='name'
                    value={searchValue}
                    placeholder="Search..."
                />
            </div>
            {showResults && (
                <div className={styles.searchResults}>
                    {loading && (
                        <div className={styles.spinner}>
                            <Loading size={20} thickness={100} />
                        </div>
                    )}
                    {error ? (
                        <div>Search error!</div>
                    ) : (
                        <>
                            {searchResults && (
                                <>
                                    {searchResults.movies.length > 0 ? (
                                        searchResults.movies.map(item => (
                                            <div key={item.id} className={styles.searchResultsItemWrapper}>
                                                <MoviesSearchItem
                                                    id={item.id}
                                                    movieName={item.name}
                                                    directorName={item.director.name}
                                                    movieImg={`${IMAGE_SRV}/poster/${item.id}`}
                                                    directorImg={`${IMAGE_SRV}/director/${item.director.id}`}
                                                    onSelect={handleSelect}
                                                />
                                            </div>
                                        ))
                                    ) : (
                                        <div>No movies found</div>
                                    )}
                                </>
                            )}
                        </>
                    )}
                </div>
            )}
        </>
    )
}
