import React, { Fragment, useReducer } from 'react'
import { useQuery, useMutation } from '@apollo/client'
import { useSearchParams, useNavigate } from 'react-router-dom'

import { Table, ButtonDropdown, Pagination, Text } from '@geist-ui/core'
import { XCircle, Edit, MoreHorizontal, ChevronRight, ChevronLeft, Plus } from '@geist-ui/icons'

import { MoviesSearch } from '../MoviesSearch/MoviesSearch'
import { MoviesForm } from '../MoviesForm/MoviesForm'
import { ConfirmDeleteDialog } from '../../ConfirmDeleteDialog/ConfirmDeleteDialog'
import { FilterValue } from '../../FilterValue/FilterValue'
import { SortControl } from '../../SortControl/SortControl'
import { NewRating as Rating } from '../../Rating/NewRating'
import { Img } from '../../Img/Img'
import { Loading } from '../../Loading/Loading'

import { moviesQuery, genresQuery, moviesPageCountQuery } from '../../../queries/queries'
import { deleteMovieMutation, updateRatingMutation } from '../../../queries/mutations'
import { sortObjectToString, makeSearchParams } from '../../../utils'
import { IMAGE_SRV } from '../../../constants'

import moviePlaceholder from '../../../images/movie.png'

import styles from './styles.module.scss'

const initialMovieData = {
    id: null,
    name: '',
    genres: [],
    watched: false,
    rate: 0,
    director: {
        id: null,
        name: '',
    },
    year: "2022",
    description: '',
}

const initialState = {
    firstTimeRender: true,
    movieData: initialMovieData,
    openEditDialog: false, // Модальное окно редактирования элемента
    openDeleteDialog: false, // Модальное окно удаления элемента
    sort: { year: "", rate: "" },
}

const reducer = (state, action) => {
    switch (action.type) {
        case 'resetFirstTimeRender': {
            return { ...state, firstTimeRender: false }
        }
        case 'setMovieData': {
            return { ...state, movieData: action.payload }
        }
        case 'openEditDialog': {
            return { ...state, openEditDialog: true }
        }
        case 'closeEditDialog': {
            return { ...state, openEditDialog: false }
        }
        case 'openDeleteDialog': {
            return { ...state, openDeleteDialog: true }
        }
        case 'closeDeleteDialog': {
            return { ...state, openDeleteDialog: false }
        }
        case 'setSortMode': {
            return { ...state, sort: action.payload }
        }
        case 'setDescription': {
            return { ...state, movieData: { ...state.movieData, description: action.payload } }
        }
        case 'setDirector': {
            return { ...state, movieData: { ...state.movieData, director: action.payload }}
        }
        case 'setRate': {
            return { ...state, movieData: { ...state.movieData, rate: action.payload }}
        }
        case 'setYear': {
            return { ...state, movieData: { ...state.movieData, year: action.payload }}
        }
        case 'setName': {
            return { ...state, movieData: { ...state.movieData, name: action.payload }}
        }
        case 'setGenres': {
            return { ...state, movieData: { ...state.movieData, genres: action.payload }}
        }
        default: {
            throw new Error(`Invalid action '${action.type}'`)
        }
    }
}

export const MoviesTable = (props) => {
    const [state, dispatch] = useReducer(reducer, initialState)

    const [searchParams] = useSearchParams()
    const navigate = useNavigate()

    const currentPage = searchParams.get('page') ? Number(searchParams.get('page')) : 1

    const { data: moviesPageCountData, loading: moviesPageCountLoading, error: moviesPageCountError } = useQuery(moviesPageCountQuery, {
        variables: { genreId: searchParams.get('genre') },
    })
    const { data: moviesData, loading: moviesLoading, error: moviesError, refetch: moviesRefetch, fetchMore } = useQuery(moviesQuery, {
        variables: { genreId: searchParams.get('genre'), sort: sortObjectToString(state.sort), page: Number(currentPage)-1 },
    })
    const { data: genresData, loading: genresLoading, error: genresError } = useQuery(genresQuery, {
        variables: { genreId: searchParams.get('genre') },
    })

    const [updateRating, updateRatingResult] = useMutation(updateRatingMutation)

    const handleDeleteDialogOpen = () => { dispatch({ type: 'openDeleteDialog' }) }
    const handleDeleteDialogClose = () => { dispatch({ type: 'closeDeleteDialog' }) }

    const handleDelete = (movieData) => {
        dispatch({ type: 'setMovieData', payload: movieData })
        handleDeleteDialogOpen()
    }

    const handleEditDialogOpen = (movieData = null) => {
        dispatch({ type: 'setMovieData', payload: movieData })
        dispatch({ type: 'openEditDialog' })
    }

    const handleEditDialogClose = (id) => {
        dispatch({ type: 'closeEditDialog' })
        dispatch({ type: 'setMovieData', payload: initialMovieData })
        // Перезагружаем изображение (добавляем ненужный параметр в src для обхода кэша)
        const movieImg = document.getElementById(id)
        if (movieImg) {
            const newSrc = `${IMAGE_SRV}/poster/${id}?_=${new Date().getTime()}`
            setTimeout(() => {
                movieImg.setAttribute('src', newSrc)
            }, 1000)
        }
    }

    // Закрываем (убираем) фильтр по жанру
    const handleCloseFilter = (genreId) => {
        if(genreId) {
            const genre = searchParams.get('genre')
            const genreArray = genre ? genre.split(',') : []
            // Удаляем genreId из списка
            genreArray.splice(genreArray.findIndex(item => item === genreId), 1)
            if (genreArray.length === 0) {
                navigate('/movies')
                return
            }
            const newGenre = genreArray.join(',')

            const paramsString = makeSearchParams({
                genre: newGenre,
                page: currentPage,
            })
            navigate(`/movies${paramsString}`)
        }
    }

    // Добавляем фильтр по жанру
    const handleGenreClick = (e) => {
        e.stopPropagation()
        const genreId = e.target.dataset.genreid
        if (genreId) {
            const genre = searchParams.get('genre')
            const genreArray = genre ? genre.split(',') : []
            // Если в списке нет genreId, то добавляем
            if (!genreArray.find(item => item === genreId)) {
                genreArray.push(genreId)
            }
            const newGenre = genreArray.join(',')

            const paramsString = makeSearchParams({
                genre: newGenre,
                page: currentPage
            })
            navigate(`/movies${paramsString}`)
        }
    }

    // Переход на другую страницу
    const handlePageChange = async (newPage) => {
        // Обходим глюк (баг) в компоненте Pagination, когда при рендере вызывается handlePageChange(1) при любой начальной странице
        if (state.firstTimeRender) {
            dispatch({ type: 'resetFirstTimeRender' })
            return
        }
        if (currentPage !== newPage) {
            const paramsString = makeSearchParams({
                genre: searchParams.get('genre'),
                page: newPage,
            })
            navigate(`/movies${paramsString}`)
        }
    }

    // Сортировка
    const handleChangeSortMode = async (field, mode) => {
        const sortMode = state.sort
        // Сохраняем строку сортировки для поля field
        sortMode[field] = mode
        // Сохраняем в стейт
        dispatch({ type: 'setSortMode', payload: sortMode })
    }

    // Рейтинг
    const handleRating = async (id, rate) => {
        await updateRating({
            variables: {
                id,
                rate,
            },
            refetchQueries: [{
                query: moviesQuery,
                variables: {
                    genreId: searchParams.get('genre'), sort: sortObjectToString(state.sort), page: currentPage-1,
                },
            }],
        })
    }

    // Клик по строке
    const handleMovieClick = (data) => {
        navigate(`/movie?movie=${data.id}`)
    }

    // Клик по режиссеру
    const handleDirectorClick = (e) => {
        e.stopPropagation()
        const directorId = e.target.dataset.directorid
        if (directorId) {
            navigate(`/director?director=${directorId}`)
        }
    }

    if (moviesError || genresError || moviesPageCountError) {
        return null
    }

    const genreFilter = searchParams.get('genre')

    const tableData = !moviesData ? [] : moviesData.movies.map((item) => {
        return {
            id: item.id,
            poster: (
                <Img src={`${IMAGE_SRV}/poster/${item.id}`} placeholder={moviePlaceholder} width="100px" height="133px" id={item.id} />
            ),
            name: item.name,
            year: item.year,
            rate: <Rating id={item.id} value={item.rate} rateChange={handleRating} />,
            director: <span className={styles.link} data-directorid={item.director.id} onClick={handleDirectorClick}>{item.director.name}</span>,
            genres: (
                <div onClick={handleGenreClick}>
                    {item.genres.map(item1 => (
                        <Fragment key={item1.id}>
                            <span className={styles.link} data-genreid={item1.id}>{item1.name}</span><br />
                        </Fragment>
                    ))}
                </div>
            ),
            actions: (
                <ButtonDropdown auto icon={<MoreHorizontal />} >
                    <ButtonDropdown.Item main onClick={() => handleEditDialogOpen(item)} className={styles.menuItem}>
                        <Edit />
                        <div>Edit</div>
                    </ButtonDropdown.Item>
                    <ButtonDropdown.Item onClick={() => handleDelete(item)} className={styles.menuItem}>
                        <XCircle />
                        <div>Delete</div>
                    </ButtonDropdown.Item>
                </ButtonDropdown>
            )
        }
    })

    return (
        <>
            {(moviesLoading || genresLoading || moviesPageCountLoading) && (
                <div className={styles.spinner}>
                    <Loading />
                </div>
            )}
            {genresData && (
                <MoviesForm 
                    selectedValue={state.movieData || initialMovieData}
                    open={state.openEditDialog}
                    genresData={genresData}
                    dispatchFunc={dispatch}
                    handleEditDialogClose={handleEditDialogClose}
                    refetchQuery={moviesRefetch}
                />
            )}
            <ConfirmDeleteDialog 
                open={state.openDeleteDialog}
                handleClose={handleDeleteDialogClose}
                dataToDelete={state.movieData}
                deleteMutation={deleteMovieMutation}
                refetchFunc={moviesRefetch}
            />
            <div className={styles.tableContainer}>
                <div className={styles.searchRoot}>
                    <MoviesSearch />
                    {genreFilter && genresData && genresData.genres.map(item => (
                        <Fragment key={item.id}>
                            <div className={styles.horizontalSpace}></div>
                            <FilterValue data={item.name} dataId={item.id} closeAction={handleCloseFilter} />
                        </Fragment>
                    ))}
                </div>
                <Table data={tableData} rowClassName={()=>styles.tableRow} onRow={handleMovieClick}>
                    <Table.Column prop="poster" label="Poster" />
                    <Table.Column prop="name" label="Name" />
                    <Table.Column prop="year">
                        <div className={styles.sortContainer}>
                            <Text>Year</Text>&nbsp;<SortControl field="year" onChangeSortMode={handleChangeSortMode} />
                        </div>
                    </Table.Column>
                    <Table.Column prop="rate">
                        <div className={styles.sortContainer}>
                            <Text>Rate</Text>&nbsp;<SortControl field="rate" onChangeSortMode={handleChangeSortMode} />
                        </div>
                    </Table.Column>
                    <Table.Column prop="director" label="Director" />
                    <Table.Column prop="genres" label="Genres" />
                    <Table.Column prop="actions" label="" />
                </Table>
            </div>
            <div className={styles.pagination}>
                {moviesPageCountData && (
                    <Pagination count={moviesPageCountData.moviesPageCount} onChange={handlePageChange} page={currentPage}>
                        <Pagination.Next><ChevronRight /></Pagination.Next>
                        <Pagination.Previous><ChevronLeft /></Pagination.Previous>
                    </Pagination>
                )}
            </div>
            <div onClick={handleEditDialogOpen} color="primary" aria-label="Add" className={styles.fab}>
                <Plus />
            </div>
        </>
    )
}

export default MoviesTable
