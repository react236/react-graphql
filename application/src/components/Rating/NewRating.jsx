import React, { useState } from "react"

import { Star } from '@geist-ui/icons'

import styles from './styles.module.scss'

/**
 * @param count - количество звездочек, от 5 
 * @param id - уникальный ID компонента
 * @param value - начальное значение рейтинга
 * @param rateChange(id, value) - коллбэк при изменении рейтинга
 * @returns React.Element
 */
export const NewRating = ({ count = 5, id, value = 1, rateChange }) => {
    const [ hover, setHover ] = useState(false)
    const [ hoverStarIndex, setHoverStarIndex ] = useState(0)

    let realCount = count < 5 ? 5 : count
    realCount = realCount > 9 ? 9 : realCount

    let realValue = value < 1 ? 1 : value
    realValue = realValue > 9 ? 9 : realValue

    const handleContainerEnter = e => {
        setHover(true)
    }

    const handleContainerLeave = e => {
        setHover(false)
    }

    const handleMouseEnter = (e, index) => {
        setHoverStarIndex(index)
    }

    const handleMouseLeave = (e, index) => {
        setHoverStarIndex(index)
    }

    const handleStarClick = (e, id, index) => {
        e.stopPropagation()
        rateChange(id, index)
    }

    return (
        <div 
            className={styles.container}
            onMouseEnter={handleContainerEnter}
            onMouseLeave={handleContainerLeave}
        >
            {Array(realCount).fill(null).map((item, index) => {
                const fillCount = hover ? hoverStarIndex : realValue-1
                const fillColor = index <= fillCount ? "black" : "none"

                return (
                    <span
                        key={index}
                        className={styles.star}
                        onMouseEnter={e => handleMouseEnter(e, index)}
                        onMouseLeave={e => handleMouseLeave(e, index)}
                        onClick={(e) => handleStarClick(e, id, index+1)}
                    >
                        <Star fill={fillColor} />
                    </span>
                )
            })}
        </div>
    )
}
