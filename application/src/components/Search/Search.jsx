import React from 'react'

import { Input } from '@geist-ui/core'
import { Search as SearchIcon } from '@geist-ui/icons'

import styles from './styles.module.scss'

export const Search = (props) => {

    const { name, value, handleChange, handleSearch } = props

    return (
        <div className={styles.search}>
            <div className={styles.searchIcon}>
                <SearchIcon />
            </div>
            <Input
                onChange={handleChange}
                onKeyDown={handleSearch}
                name={name}
                value={value}
                placeholder="Search…"
            />
        </div>
    )
}
