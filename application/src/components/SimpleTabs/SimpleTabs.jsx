import React, { useEffect } from 'react'
import { useLazyQuery } from '@apollo/client'
import { Link, Route, Routes, useLocation, useNavigate } from 'react-router-dom'
import { Film, User as UserIcon, Columns } from '@geist-ui/icons'

import useGlobalState from '../../state'
import { User } from '../User/User'
import { TabItem } from './TabItem/TabItem';
import { userByTokenQuery } from '../../queries/queries'

import Movies from '../../routes/movies'
import Directors from '../../routes/directors'
import Geners from '../../routes/genres'
import Movie from '../../routes/movie'
import Director from '../../routes/director'
import Auth from '../../routes/auth'
import Logout from '../../routes/logout'

import styles from './styles.module.scss'

const SimpleTabs = (props) => {
	const [token, setToken] = useGlobalState('token')
	const [login, setLogin] = useGlobalState('login')

	let location = useLocation()
	const navigate = useNavigate()

	const [getLogin, getLoginResult] = useLazyQuery(userByTokenQuery)

	useEffect(async () => {
		// Если токен пустой, переходим на страницу авторизации
		if(!token && location.pathname !== '/login') {
			navigate('/login')
		} else if (token) {
			// Запрос информации о пользователе
			const result = await getLogin({
				variables: { token: token }
			})
			if (result) setLogin(result.data.userByToken)
		}
	}, [token, location])

	return (
		<div className={styles.root}>
			<div className={styles.menuContainer}>
				<div className={styles.menu}>
					<Link to='/movies'><TabItem icon={<Film />} label='Movies' /></Link>
					<Link to='/directors'><TabItem icon={<UserIcon />} label='Directors' /></Link>
					<Link to='/genres'><TabItem icon={<Columns />} label='Genres' /></Link>
				</div>
				<User />
			</div>
			<Routes>
				<Route path="/" element={<Movies />} />
				<Route path='movies' element={<Movies />} />
				<Route path='directors' element={<Directors />} />
				<Route path='genres' element={<Geners />} />
				<Route path='movie' element={<Movie />} />
				<Route path='director' element={<Director />} />
				<Route path='login' element={<Auth />} />
				<Route path='logout' element={<Logout />} />
			</Routes>
		</div>
	)
}

export default SimpleTabs
