import React from "react";

import styles from './styles.module.scss'

export const TabItem = ({ icon, label, className = '' }) => {
    return (
        <div className={`${styles.container} ${className}`}>
            {icon}&nbsp;&nbsp;{label}
        </div>
    )
}
