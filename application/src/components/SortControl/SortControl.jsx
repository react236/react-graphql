import React, { useState } from 'react'

import { ChevronUp, ChevronDown, ChevronUpDown } from '@geist-ui/icons'

import styles from './styles.module.scss'

export const SortControl = ({ field, onChangeSortMode }) => {
    const [sortMode, setSortMode] = useState(0) // 0 - no sort, 1 - ASC, 2 - DESC

    const handleSortChange = () => {
        let newSortMode = 0

        if (sortMode < 2) {
            newSortMode = sortMode + 1
        }

        setSortMode(newSortMode)

        let mode = ''

        switch (newSortMode) {
            case 1: {
                mode = field
                break
            }
            case 2: {
                mode = `-${field}`
                break
            }
            default: {
                break
            }
        }
        onChangeSortMode(field, mode)
    }

    return (
        <div className={styles.container}>
            {sortMode === 0 && <ChevronUpDown onClick={handleSortChange} />}
            {sortMode === 1 && <ChevronUp color="#0070F3" onClick={handleSortChange} />}
            {sortMode === 2 && <ChevronDown color="#0070F3" onClick={handleSortChange} />}
        </div>
    )
}
