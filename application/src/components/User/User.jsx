import React from 'react'
import { Link } from 'react-router-dom'
import { LogOut } from '@geist-ui/icons'
import { Tag } from '@geist-ui/core'

import { TabItem } from '../SimpleTabs/TabItem/TabItem.jsx'
import useGlobalState from '../../state.js'

import styles from './styles.module.scss'

export const User = () => {
    const [token] = useGlobalState('token')
    const [login] = useGlobalState('login')

    return (
        <>
            {token ? (
                <div className={styles.container}>
                    <Tag type="success">{login}</Tag>
                    <Link to='/logout'><TabItem icon={<LogOut />} label='Logout' /></Link>
                </div>
            ) : null}
        </>
    )
}
