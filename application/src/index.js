import React from 'react';
import ReactDOM from 'react-dom';

import { ApolloClient, ApolloProvider, InMemoryCache, HttpLink, from, gql } from '@apollo/client'
import { onError } from '@apollo/client/link/error'
import { setContext } from '@apollo/client/link/context'

import { BrowserRouter } from "react-router-dom"
import toast, { Toaster } from 'react-hot-toast'
import { IMAGE_SRV } from './constants'

import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

const errorLink = onError(({ graphQLErrors, networkError }) => {
    if (networkError) {
        console.log(`[Network Error]: ${networkError}`);
        toast.error('Data loading error', {
            duration: 10000,
            position: 'bottom-center',
        })
    }
    if (graphQLErrors) {
        graphQLErrors.forEach( ({ message, locations, path }) => {
            toast.error(`Error: ${message}`, { position: 'bottom-center' })
        })
    }
})

// const uri = 'http://localhost:5000/graphql'
const uri = `${IMAGE_SRV}/graphql`
const httpLink = new HttpLink({ uri })

const authLink = setContext((_, { headers }) => {
    // get the authentication token from local storage if it exists
    const token = localStorage.getItem('react-graphql-user-token')
    // return the headers to the context so httpLink can read them
    return {
        headers: {
            ...headers,
            authorization: token ? `Bearer ${token}` : "",
        }
    }
})

const client = new ApolloClient({
    //uri: 'http://localhost:5000/graphql',
    cache: new InMemoryCache(),
    link: from([authLink, errorLink, httpLink]),
    connectToDevTools: true,
})

ReactDOM.render(
    <ApolloProvider client={client}>
        <BrowserRouter>
            <App />      
        </BrowserRouter>
        <Toaster />
    </ApolloProvider>, 
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
