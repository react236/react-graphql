import { gql } from '@apollo/client'

export const addDirectorMutation = gql`
    mutation addDirector($name: String!, $birthday: String!) {
        addDirector(name: $name, birthday: $birthday) {
            id
            name
            birthday
        }
    }
`

export const addMovieMutation = gql`
    mutation addMovie($name: String!, $genres: [ID], $watched: Boolean!, $rate: Int, $year: Int!, $directorId: ID, $description: String) {
        addMovie(name: $name, genres: $genres, watched: $watched, rate: $rate, year: $year, directorId: $directorId, description: $description) {
            id
            name
            genres {
                name
            }
            watched
            rate
            year
            director {
                name
                birthday
            }
            description
        }
    }
`

export const addGenreMutation = gql`
    mutation addGenre($name: String!) {
        addGenre(name: $name) {
            name
        }
    }
`

export const addUserMutation = gql`
    mutation addUser($login: String!, $pwd: String!, $email: String!) {
        addUser(login: $login, pwd: $pwd, email: $email) {
            id
            login
            token
        }
    }
`

export const deleteMovieMutation = gql`
    mutation deleteMovie($id: ID) {
        deleteMovie(id: $id) {
            id
        }
    }
`

export const deleteDirectorMutation = gql`
    mutation deleteDirector($id: ID) {
        deleteDirector(id: $id) {
            id
        }
    }
`

export const deleteGenreMutation = gql`
    mutation deleteGenre($id: ID) {
        deleteGenre(id: $id) {
            id
        }
    }
`

export const updateDirectorMutation = gql`
    mutation updateDirector($id: ID, $name: String!, $birthday: String!) {
        updateDirector(id: $id, name: $name, birthday: $birthday) {
            id
            name
            birthday
        }
    }
`

export const updateMovieMutation = gql`
    mutation updateMovie($id: ID, $name: String!, $genres: [ID], $watched: Boolean!, $rate: Int, $year: Int!, $directorId: ID, $description: String) {
        updateMovie(id: $id, name: $name, genres: $genres, watched: $watched, rate: $rate, year: $year, directorId: $directorId, description: $description) {
            id
            name
            genres {
                name
            }
            watched
            rate
            year
            director {
                name
                birthday
            }
            description
        }
    }
`

export const updateGenreMutation = gql`
    mutation updateGenre($id: ID, $name: String!) {
        updateGenre(id: $id, name: $name) {
            id
            name
        }
    }
`

export const updateRatingMutation = gql`
    mutation updateRating($id: ID!, $rate: Int!) {
        updateRating(id: $id, rate: $rate) {
            id
            rate
        }
    }
`
