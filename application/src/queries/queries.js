import { gql } from '@apollo/client'

export const moviesQuery = gql`
    query moviesQuery($name: String, $genreId: String, $page: Int, $sort: String) {
        movies(name: $name, genreId: $genreId, page: $page, sort: $sort) {
            id
            name
            watched
            rate
            year
            director {
                id
                name
            }
            genres {
                id
                name
            }
            description
        }
    }
`

export const moviesSearchQuery = gql`
    query moviesSearchQuery($name: String) {
        movies(name: $name) {
            id
            name
            director {
                id
                name
            }
        }
    }
`

export const moviesPageCountQuery = gql`
    query moviesPageCountQuery($genreId: String) {
        moviesPageCount(genreId: $genreId)
    }
`

export const movieQuery = gql`
    query movieQuery($id: ID) {
        movie(id: $id) {
            id
            name
            watched
            rate
            year
            director {
                id
                name
            }
            genres {
                id
                name
            }
            description
        }
    }
`

export const directorsQuery = gql`
    query directorsQuery($name: String, $page: Int) {
        directors(name: $name, page: $page) {
            id
            name
            birthday
            movies {
                id
                name
            }
        }
    }
`

export const directorsForAddMovieQuery = gql`
    query directorsQuery($name: String) {
        directors(name: $name) {
            id
            name
        }
    }
`

export const directorsSearchQuery = gql`
    query directorsSearchQuery($name: String) {
        directors(name: $name) {
            id
            name
            birthday
        }
    }
`

export const directorsPageCountQuery = gql`
    query directorsPageCountQuery {
        directorsPageCount
    }
`

export const directorQuery = gql`
    query directorQuery($id: ID) {
        director(id: $id) {
            id
            name
            birthday
            movies {
                id
                name
                rate
                year
                genres {
                    id
                    name
                }
            }
        }
    }
`

export const genresQuery = gql`
    query genresQuery($name: String, $genreId: String) {
        genres(name: $name, genreId: $genreId) {
            id
            name
        }
    }
`

export const userQuery = gql`
    query user($login: String) {
        user(login: $login) {
            id
            login
            email
        }
    }
`

export const userByTokenQuery = gql`
    query userByTokenQuery($token: String!) {
        userByToken(token: $token)
    }
`

export const loginQuery = gql`
    query login($login: String!, $pwd: String!) {
        login(login: $login, pwd: $pwd) {
            login
            token
        }
    }
`
