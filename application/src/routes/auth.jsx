import React from "react"

import { AuthComponent } from '../components/Auth/AuthComponent'

const Auth = () => {
    return <AuthComponent />
}

export default Auth
