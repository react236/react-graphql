import React from "react"

import { DirectorComponent } from "../components/Director/DirectorComponent"

const Director = () => {
    return <DirectorComponent />
}

export default Director
