import React from "react"

import DirectorsComponent from "../components/Directors/DirectorsComponent"

const Directors = () => {
    return <DirectorsComponent />
}

export default Directors
