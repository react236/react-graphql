import React from "react"

import GenresComponent from "../components/Genres/GenresComponent"

const Genres = () => {
    return <GenresComponent />
}

export default Genres
