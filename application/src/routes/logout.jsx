import React from "react"

import { Logout as LogoutComponent} from '../components/Auth/Logout/Logout'

const Logout = () => {
    return <LogoutComponent />
}

export default Logout
