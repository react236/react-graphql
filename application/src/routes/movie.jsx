import React from "react"

import { MovieComponent } from "../components/Movie/MovieComponent"

const Movie = () => {
    return <MovieComponent />
}

export default Movie
