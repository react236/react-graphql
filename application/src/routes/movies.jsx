import React from "react"

import MoviesComponent from "../components/Movies/MoviesComponent"

const Movies = () => {
    return <MoviesComponent />
}

export default Movies
