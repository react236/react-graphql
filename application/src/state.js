import { createGlobalState } from 'react-hooks-global-state'

const token = localStorage.getItem('react-graphql-user-token')

const initialState = { token: token ? token : '', login: '' }

const { useGlobalState } = createGlobalState(initialState)

export default useGlobalState
