export const addLeadZero = (value, count = 2) => {
    const strValue = String(value)
    if (strValue.length >= count) {
        return strValue
    }

    const zerosCount = count - strValue.length
    const zeros = (new Array(zerosCount)).fill('0')

    return `${zeros}${strValue}`
}

// Преобразовываем объект сотритовки в строку
export const sortObjectToString = (obj) => {
    let str = ''

    for(const item in obj) {
        str = `${str} ${obj[item]}`
    }

    return str
}

// Формируем строку параметров из переданного объекта
export const makeSearchParams = (params) => {
    let paramString = '?'
    // Перебираем переданный объект и добавляем в параметры
    for (const param in params) {
        if (params[param]) {
            paramString = paramString + param + '=' + params[param] + '&'
        }
    }

    // Если ничего не добавили, возвращаем пустую строку
    if (paramString.length === 1) {
        return ''
    }

    // Убираем последний символ '&'
    paramString = paramString.slice(0, paramString.length-1)

    return paramString
}
