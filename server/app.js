// https://www.youtube.com/watch?v=GMJNSBur-lM

const express = require('express')
const { graphqlHTTP } = require('express-graphql')
const mongoose = require('mongoose')
const cors = require('cors')

const fs = require('fs/promises')
const { constants, unlink, writeFile } = require('fs')

const bodyParser = require('body-parser')
const fileUpload = require('express-fileupload');

const schema = require('./schema/schema')
const { log } = require('console')

const app = express()
const PORT = process.env.PORT || 5000

mongoose.connect('mongodb+srv://Mur77:Aq111111@cluster0.rukck.mongodb.net/graphql-tutorial?retryWrites=true&w=majority', {
    useUnifiedTopology: true
})
const dbConnection = mongoose.connection
dbConnection.on('error', err => {
    console.log(`DB connection error: ${err}`)
    process.exit(1)
})
dbConnection.once('open', () => console.log('Connected to DB!'))

app.use(cors())
app.use(fileUpload())

// create parser
const rawParser = bodyParser.raw({ type: 'application/octet-stream', limit: '1mb' })

app.use('/graphql', graphqlHTTP({
    schema,
    graphiql: true,
}))

/**
 * Отдаем постер для фильма
 */
app.get('/poster/:posterId', async function(req, res) {
    const posterId = req.params.posterId;
    const fileName = __dirname + '/image/poster/' + posterId + '.jpg';

    try {
        await fs.access(fileName, constants.F_OK);
        res.sendFile(fileName);
    } catch(e) {
        res.status(404).send(e.message);
    }
})

/**
 * Отдаем фото режиссера
 */
app.get('/director/:directorId', async function(req, res) {
    const directorId = req.params.directorId;
    const fileName = __dirname + '/image/director/' + directorId + '.jpg';

    try {
        await fs.access(fileName, constants.F_OK);
        res.sendFile(fileName);
    } catch(e) {
        res.status(404).send(e.message);
    }
})

/**
 * Принимаем постер для фильма
 */
app.post('/poster/:posterId', rawParser, async function(req, res) {
    const posterId = req.params.posterId;
    const fileName = __dirname + '/image/poster/' + posterId + '.jpg';

    console.log('Got poster for', posterId);

    try {
        await fs.writeFile(fileName, req.body);
    } catch (e) {
        res.status(500).send(e.message);
        return;
    }

    res.status(200).send('Done');
})

/**
 * Принимаем фото режиссера
 */
app.post('/director/:directorId', rawParser, async function(req, res) {
    const directorId = req.params.directorId;
    const fileName = __dirname + '/image/director/' + directorId + '.jpg';

    console.log('Got poster for director', directorId);

    try {
        await fs.writeFile(fileName, req.body);
    } catch (e) {
        res.status(500).send(e.message);
        return;
    }

    res.status(200).send('Done');
})

/**
 * Принимаем видео (частями)
 */
app.post('/movie/upload/:movieId', async function(req, res) {
    // Проверяем, есть ли файлы в запросе
    if (!req.files || Object.keys(req.files).length === 0) {
        return res.status(400).send('No files were uploaded.');
    }

    // Проверяем уникальный id
    const uniqueUploadId = req.headers['x-unique-upload-id'];
    if (!uniqueUploadId) {
        return res.status(400).send('x-unique-upload-id must be specified.');
    }

    const baseFolder = '/uploads';
    const rangePattern = /^bytes (\d+)-(\d+)\/(-1|\d+)$/g;
    const filePattern = /([0-9a-f]{7,9}.[0-9a-f]{4,7}),([0-9,\,]*)/g;
    const chunkSize = 1048576;
    const newFile = req.files.file;
    const range = req.headers['content-range'];
    const tableFilePath = __dirname + baseFolder + '/table.dat';
    const movieId = req.params.movieId;

    try {
        await fs.access(tableFilePath, constants.F_OK || constants.R_OK);
    } catch(e) {
        if (e.code === 'ENOENT') {
            // Файла нет, создаем
            //log('Create table file');
            await fs.writeFile(tableFilePath, '');
        } else {
            res.status(400).send(e.message);
            return;
        }
    }

    // --- Читаем файл в массив
    let tableData;

    // Читаем файл в tableData
    try {
        tableData = await fs.readFile(tableFilePath, { encoding: 'ASCII' });
    } catch(e) {
        res.status(400).send(e.message);
        return;
    }

    //log('--------------------------------------');
    //log('147 read table file data', tableData);

    // Распарсиваем строки
    const tableParseResult = [ ...tableData.matchAll(filePattern) ];

    // Преобразовываем tableParseResult в tableArray вида { id: uniqueUploadId, chunks: [0, 1, 2, 4, 5] }
    const tableArray = tableParseResult.map(item => {
        //log('154 parsed file data', item[1], item[2]);
        return {
            id: item[1],
            chunks: item[2].split(',').map(item1 => Number(item1)).sort((a, b) => a-b),
            totalChunks: 0,
        }
    })

    //log('159 Initial table array', tableArray);

    // --- Есть ли в массиве вновь пришедший unique-upload-id
    let index = tableArray.findIndex(item => item.id === uniqueUploadId);
    if (index === -1) {
        // Добавляем новый id в tableArray
        tableArray.push({ id: uniqueUploadId, chunks: [], totalChunks: 0 })
        index = tableArray.length-1;
    }

    //log('169 tableArray1', tableArray);
    //log('index', index);

    // --- Добавляем пришедший пакет соответствующему элементу в tableArray
    // Парсим content-range
    const contentRange = [ ...range.matchAll(rangePattern) ];
    const contentStart = contentRange[0][1];
    const contentEnd = contentRange[0][2];
    const contentTotal = contentRange[0][3];

    // Номер пакета
    const chunkNumber = contentStart / chunkSize;

    tableArray[index].chunks[chunkNumber] = chunkNumber;

    //log('184 TableArray', tableArray)
    // Сохраняем пакет в файл
    try {
        const fileName = __dirname + baseFolder + '/' + uniqueUploadId + '-' + chunkNumber + '.dat';
        await fs.writeFile(fileName, newFile.data);
    } catch (e) {
        res.status(400).send('188 '+e.message);
        return;
    }

    // Если пришло поле contentTotal, сохраняем в таблице
    if (Number(contentTotal) !== -1) {
        tableArray[index].totalChunks = Math.ceil(contentTotal / chunkSize);
        //log('Got contentTotal', tableArray[index].totalChunks);
    }
    
    // --- Собираем пакеты в один файл
    const totalChunks = tableArray[index].totalChunks;
    // Проверяем, все ли пакеты пришли
    //log('Test all chunks', totalChunks, tableArray[index].chunks.length);
    if (totalChunks && tableArray[index].chunks.length === totalChunks) {
        //log('Got all packets');
        // Имя файла
        const fileName = __dirname + baseFolder + '/' + movieId + '.mp4';
        let data;
        let fileRemoved = false;
        // Собираем пакеты
        for(let i=0; i<totalChunks; i++) {
            const chunkFileName = __dirname + baseFolder + '/' + uniqueUploadId + '-' + i + '.dat';
            try {
                data = await fs.readFile(chunkFileName);
                if (!fileRemoved) {
                    await fs.writeFile(fileName, data);
                    fileRemoved = true;
                } else {
                    await fs.appendFile(fileName, data);
                }
            } catch(e) {
                res.status(400).send(e.message);
                fs.unlink(fileName);
                return;
            }
        }
        // Перемещаем файл в /image/video
        const newFilename = __dirname + '/image/video/' + movieId + '.mp4';
        await fs.rename(fileName, newFilename);
        // Удаляем файлы пакетов
        for(let i=0; i<totalChunks; i++) {
            const chunkFileName = __dirname + baseFolder + '/' + uniqueUploadId + '-' + i + '.dat';
            //log('file to remove', chunkFileName);
            await fs.unlink(chunkFileName);
        }
        // Удаляем элемент таблицы
        tableArray.splice(index, 1);
    }

    // --- Сохраняем таблицу
    // Преобразовываем таблицу в строку
    let tableToFile = '';
    for (let i=0; i<tableArray.length; i++) {
        tableToFile = tableToFile + tableArray[i].id;

        for (let j=0; j<tableArray[i].chunks.length; j++) {
            tableToFile = tableToFile + ',' + String(tableArray[i].chunks[j]);
        }
        // Добавляем символ "конец строки"
        tableToFile = tableToFile + '\n';
    }

    //log('table file data to store', tableToFile);

    // Сохраняем файл таблицы
    try {
        await fs.writeFile(tableFilePath, tableToFile);
    } catch (e) {
        res.status(400).send(e.message);
        return;
    }

    res.status(200).send('OK');
})

/**
 * Отдаем видео
 */
app.get('/movie/:movieId', async function(req, res) {
    const movieId = req.params.movieId;
    const fileName = __dirname + '/image/video/' + movieId + '.mp4';

    try {
        await fs.access(fileName, constants.F_OK);
        res.sendFile(fileName);
    } catch(e) {
        res.status(404).send(e.message);
    }
})

app.listen(PORT, () => {
    console.log(`Server started at port ${PORT}`);
})
