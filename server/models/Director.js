const mongoose = require('mongoose')

const { Schema } = mongoose

const directorSchema = new Schema({
    name: String,
    birthday: { type: String, default: 'Wed Feb 27 1963 23:00:00 GMT+0200' },
})

module.exports = mongoose.model('Director', directorSchema)
