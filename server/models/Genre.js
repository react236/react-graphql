const mongoose = require('mongoose')

const { Schema, model } = mongoose

const genreSchema = new Schema({
    name: String,
})

module.exports = model('Genre', genreSchema)
