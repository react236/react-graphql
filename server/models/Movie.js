const mongoose = require('mongoose')

const { Schema, model } = mongoose

const movieSchema = new Schema({
    name: String,
    directorId: String,
    rate: Number,
    watched: Boolean,
    year: Number,
    description: String,
})

module.exports = model('Movie', movieSchema)
