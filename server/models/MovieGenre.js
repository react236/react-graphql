// Таблица связи "многие ко многим"

const mongoose = require('mongoose')

const { Schema, model } = mongoose

const movieGenreSchema = new Schema({
    movieId: String,
    genreId: String,
})

module.exports = model('MovieGenre', movieGenreSchema)
