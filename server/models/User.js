const mongoose = require('mongoose')

const { Schema } = mongoose

const userSchema = new Schema({
    login: String,
    pwd: String,
    email: String,
})

module.exports = mongoose.model('User', userSchema)
