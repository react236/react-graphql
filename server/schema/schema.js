const graphql = require('graphql')
const fs = require('fs/promises')
const jwt = require('jsonwebtoken')

const { PAGE_LIMIT, SECRET } = require('../constants/constants')

const { GraphQLObjectType, GraphQLString, GraphQLSchema, GraphQLID, GraphQLInt, GraphQLList, GraphQLNonNull, GraphQLBoolean } = graphql

// Простая функция проверки авторизации
const isAuthorized = headers => {
    if (!headers['authorization']) {
        return false
    }
    const token = headers['authorization'].split(' ')[1]

    return !!token
}

// Модели
const Movies = require('../models/Movie')
const Directors = require('../models/Director')
const Genres = require('../models/Genre')
const MovieGenres = require('../models/MovieGenre')
const Users = require('../models/User')

// Тип данных коллекции genre
const GenreType = new GraphQLObjectType({
    name: 'Genre',
    fields: () => ({
        id: { type: GraphQLID },
        name: { type: new GraphQLNonNull(GraphQLString) },
        movies: {
            type: new GraphQLList(MovieType),
            async resolve(parent, args) {
                const mg = await MovieGenres.find({ genreId: parent.id })
                const m = mg.map(item => item.movieId)
                return await Movies.find({ _id: { $in: m } })
            }
        },
    }),
})

// Тип данных коллекции movies
const MovieType = new GraphQLObjectType({
    name: 'Movie',
    fields: () => ({
        id: { type: GraphQLID },
        name: { type: new GraphQLNonNull(GraphQLString) },
        genres: { 
            type: new GraphQLList(GenreType),
            async resolve(parent, args) {
                const mg = await MovieGenres.find({ movieId: parent.id })
                const g = mg.map(item => item.genreId)
                return await Genres.find({ _id: { $in: g } })
            }
        },
        rate: { type: GraphQLInt },
        watched: { type: new GraphQLNonNull(GraphQLBoolean) },
        year: { type: new GraphQLNonNull(GraphQLInt) },
        director: {
            type: DirectorType,
            async resolve(parent, args) {
                return await Directors.findById(parent.directorId)
            }
        },
        description: { type: GraphQLString },
    }),
})

// Тип данных коллекции directors
const DirectorType = new GraphQLObjectType({
    name: 'Director',
    fields: () => ({
        id: { type: GraphQLID },
        name: { type: new GraphQLNonNull(GraphQLString) },
        birthday: { type: new GraphQLNonNull(GraphQLString) },
        movies: {
            type: new GraphQLList(MovieType),
            async resolve(parent, args) {
                return await Movies.find({ directorId: parent.id })
            },
        },
    }),
})

// Тип данных для результатов поиска
const SearchType = new GraphQLObjectType({
    name: 'Search',
    fields: () => ({
        movies: {
            type: new GraphQLList(MovieType),
        },
        directors: {
            type: new GraphQLList(DirectorType),
        }
    })
})

// Тип данных для изменения рейтинга
const RateType = new GraphQLObjectType({
    name: 'Rate',
    fields: () => ({
        id: { type: GraphQLID },
        rate: { type: GraphQLInt },
    }),
})

// Тип данных для пользователя
const UserType = new GraphQLObjectType({
    name: 'User',
    fields: () => ({
        id: { type: GraphQLID },
        login: { type: new GraphQLNonNull(GraphQLString) },
        pwd: { type: GraphQLString },
        email: { type: GraphQLString },
    }),
})

// Тип данных для результатов логина
const LoginType = new GraphQLObjectType({
    name: 'Login',
    fields: () => ({
        login: { type: GraphQLString, },
        token: { type: GraphQLString, },
    }),
})

// Тип данных для регистрации пользователя
const AddUserType = new GraphQLObjectType({
    name: 'AddUser',
    fields: () => ({
        id: { type: GraphQLID },
        login: { type: new GraphQLNonNull(GraphQLString) },
        pwd: { type: new GraphQLNonNull(GraphQLString) },
        email: { type: new GraphQLNonNull(GraphQLString) },
        token: { type: new GraphQLNonNull(GraphQLString) },
    }),
})

const Mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: {
        addDirector: {
            type: DirectorType,
            args: {
                name: { type: new GraphQLNonNull(GraphQLString) },
                birthday: { type: new GraphQLNonNull(GraphQLString) },
            },
            async resolve(parent, args, context) {
                if (!isAuthorized(context.headers)) {
                    return new Error('Unauthorized')
                }
                const director = new Directors({
                    name: args.name,
                    birthday: args.birthday,
                })
                return await director.save()
            },
        },
        addMovie: {
            type: MovieType,
            args: {
                name: { type: new GraphQLNonNull(GraphQLString) },
                genres: { type: new GraphQLList(GraphQLID) },
                rate: { type: GraphQLInt },
                year: { type: new GraphQLNonNull(GraphQLInt) },
                watched: { type: new GraphQLNonNull(GraphQLBoolean) },
                directorId: { type: GraphQLID },
                description: { type: GraphQLString },
            },
            async resolve(parent, args, context) {
                if (!isAuthorized(context.headers)) {
                    return new Error('Unauthorized')
                }
                const movie = new Movies({
                    name: args.name,
                    rate: args.rate,
                    year: args.year,
                    watched: args.watched,
                    directorId: args.directorId,
                    description: args.description,
                })
                const result = await movie.save()

                // Добавляем жанры
                // Список новых жанров фильма
                const newGenres = args.genres.map(item => ({
                    movieId: result.id,
                    genreId: item,
                }))
                await MovieGenres.create(newGenres)
                return result
            },
        },
        addGenre: {
            type: GenreType,
            args: {
                name: { type: new GraphQLNonNull(GraphQLString) },
            },
            async resolve(parent, args, context) {
                if (!isAuthorized(context.headers)) {
                    return new Error('Unauthorized')
                }
                const genre = new Genres({
                    name: args.name,
                })
                return await genre.save()
            },
        },        
        addUser: {
            type: AddUserType,
            args: {
                login: { type: new GraphQLNonNull(GraphQLString) },
                pwd: { type: new GraphQLNonNull(GraphQLString) },
                email: { type: new GraphQLNonNull(GraphQLString) },
            },
            async resolve(parent, args, context) {
                // Проверяем, есть ли уже такой пользователь
                const userExists = await Users.findOne({ login: args.login })
                if (userExists) {
                    return new Error('User already exists')
                }
                const user = new Users({
                    login: args.login,
                    pwd: args.pwd,
                    email: args.email,
                })
                const result = await user.save()
                const token = jwt.sign({ login: args.login }, SECRET, { expiresIn: "2d" })
                return { ...result._doc, token: token }
            },
        },
        deleteDirector: {
            type: DirectorType,
            args: {
                id: { type: GraphQLID },
            },
            async resolve(parent, args, context) {
                if (!isAuthorized(context.headers)) {
                    return new Error('Unauthorized')
                }
                // Проверяем, есть ли фильм с заданным режиссером
                const movie = await Movies.find({ directorId: args.id })
                if (Array.isArray(movie) && movie.length !== 0) {
                    return null
                }
                // Удаляем режиссера
                const response = await Directors.findByIdAndRemove(args.id)
                // Удаляем фото
                const picturePath = __dirname.slice(0, -7)
                const fileName = picturePath + '/image/director/' + args.id + '.jpg'

                try {
                    await fs.unlink(fileName)
                } catch (e) {
                    console.log('Unable to remove picture', fileName)
                }

                return response
            },
        },
        deleteMovie: {
            type: MovieType,
            args: {
                id: { type: GraphQLID },
            },
            async resolve(parent, args, context) {
                if (!isAuthorized(context.headers)) {
                    return new Error('Unauthorized')
                }
                // Удаляем жанры
                await MovieGenres.deleteMany({ movieId: { $in: args.id } })
                // Удаляем фильм
                const response = await Movies.findByIdAndRemove(args.id)
                // Удаляем постер
                const posterPath = __dirname.slice(0, -7)
                const fileName = posterPath + '/image/poster/' + args.id + '.jpg'

                try {
                    await fs.unlink(fileName)
                } catch (e) {
                    console.log('Unable to remove poster ', fileName);
                }

                return response
            },
        },
        deleteGenre: {
            type: GenreType,
            args: {
                id: { type: GraphQLID },
            },
            async resolve(parent, args, context) {
                if (!isAuthorized(context.headers)) {
                    return new Error('Unauthorized')
                }
                const mg = await MovieGenres.find({ genreId: args.id })
                if (Array.isArray(mg) && mg.length === 0) {
                    return await Genres.findByIdAndRemove(args.id)
                } else {
                    return null
                }
            },
        },
        updateDirector: {
            type: DirectorType,
            args: {
                id: { type: GraphQLID }, 
                name: { type: new GraphQLNonNull(GraphQLString) },
                birthday: { type: new GraphQLNonNull(GraphQLString) },
            },
            async resolve(parent, args, context) {
                if (!isAuthorized(context.headers)) {
                    return new Error('Unauthorized')
                }
                return await Directors.findByIdAndUpdate(
                    args.id,
                    { $set: { name: args.name, birthday: args.birthday } },
                    { new: true }
                )
            },
        },
        updateMovie: {
            type: MovieType,
            args: {
                id: { type: GraphQLID }, 
                name: { type: new GraphQLNonNull(GraphQLString) },
                genres: { type: new GraphQLList(GraphQLID) },
                rate: { type: GraphQLInt },
                year: { type: new GraphQLNonNull(GraphQLInt) },
                watched: { type: new GraphQLNonNull(GraphQLBoolean) },
                directorId: { type: GraphQLID },
                description: { type: GraphQLString },
            },
            async resolve(parent, args, context) {
                if (!isAuthorized(context.headers)) {
                    return new Error('Unauthorized')
                }
                // Удаляем старые жанры
                let result = await MovieGenres.deleteMany({ movieId: { $in: args.id } })
                // Список новых жанров фильма
                const newGenres = args.genres.map(item => ({
                    movieId: args.id,
                    genreId: item,
                }))
                // Добавляем новые жанры
                result = await MovieGenres.create(newGenres)
                // Обновляем фильм
                return await Movies.findByIdAndUpdate(
                    args.id,
                    { $set: { 
                        name: args.name, 
                        rate: args.rate, 
                        year: args.year, 
                        watched: args.watched, 
                        directorId: args.directorId, 
                        description: args.description, 
                    }},
                    { new: true }
                )
            },
        },
        updateGenre: {
            type: GenreType,
            args: {
                id: { type: GraphQLID },
                name: { type: new GraphQLNonNull(GraphQLString) },
            },
            async resolve(parent, args, context) {
                if (!isAuthorized(context.headers)) {
                    return new Error('Unauthorized')
                }
                return await Genres.findByIdAndUpdate(
                    args.id,
                    { $set: { name: args.name } },
                    { new: true }
                )
            },
        },
        updateRating: {
            type: RateType,
            args: {
                id: { type: new GraphQLNonNull(GraphQLID) },
                rate: { type: new GraphQLNonNull(GraphQLInt) },
            },
            async resolve(parent, args, context) {
                if (!isAuthorized(context.headers)) {
                    return new Error('Unauthorized')
                }
                return await Movies.findByIdAndUpdate(args.id, { rate: args.rate })
            }
        }
    }
})

/**
 * Query - корневой запрос
 * movie - подзапрос, носит тип MovieType, args - какие аргументы принимает подзапрос, resolve - логика возврата нужных данных
 * 
 */
const Query = new GraphQLObjectType({
    name: 'Query',
    fields: {
        movie: {
            type: MovieType,
            args: { id: { type: GraphQLID } },
            async resolve(parent, args, context) {
                if (isAuthorized(context.headers)) {
                    return await Movies.findById(args.id)
                }

                return new Error('Unauthorized')
            },
        },
        director: {
            type: DirectorType,
            args: { id: { type: GraphQLID } },
            async resolve(parent, args, context) {
                if (isAuthorized(context.headers)) {
                    return await Directors.findById(args.id)
                }

                return new Error('Unauthorized')
            },
        },
        genre: {
            type: GenreType,
            args: { id: { type: GraphQLID } },
            async resolve(parent, args, context) {
                if (isAuthorized(context.headers)) {
                    return await Genres.findById(args.id)
                }

                return new Error('Unauthorized')
            },
        },
        user: {
            type: UserType,
            args: { login: { type: GraphQLString } },
            async resolve(parent, args, context) {
                if (!isAuthorized(context.headers)) {
                    return new Error('Unauthorized')
                }

                const result = await Users.findOne({ login: args.login.toLowerCase() })
                // Проверяем чтобы пользователь мог запросить только сам себя
                if (args.login === result.login) {
                    return result
                }

                return new Error('Unauthorized')                
            },
        },
        movies: {
            type: new GraphQLList(MovieType),
            args: { name: { type: GraphQLString }, genreId: { type: GraphQLString }, page: { type: GraphQLInt }, sort: { type: GraphQLString } },
            async resolve(parent, { name = '', genreId = '', page = 0, sort = '' }, context) {
                if (!isAuthorized(context.headers)) {
                    return new Error('Unauthorized')
                }
                let mg, movies = [], skipCount = 0
                if (page > 0) {
                    skipCount = PAGE_LIMIT * page
                }
                // Если задан genreId, отбираем все фильмы с этими жанрами
                if (genreId) {
                    genreIdArray = genreId.split(',')
                    // Выбираем фильмы по всем указанным жанрам
                    mg = await MovieGenres.find({ genreId: { $in: genreIdArray } })
                    // Формируем массив из уникальных movieId
                    const uniqueMovies = []
                    mg.forEach(item => {
                        if (!uniqueMovies.find(item1 => item.movieId === item1.movieId)) {
                            uniqueMovies.push({ movieId: item.movieId, genresCount: 0 })
                        }
                    })
                    // Считаем кол-во жанров для каждого фильма
                    mg.forEach(item => {
                        const moviePos = uniqueMovies.findIndex(item1 => item.movieId === item1.movieId)
                        uniqueMovies[moviePos].genresCount = uniqueMovies[moviePos].genresCount + 1
                    })
                    // Выбираем элементы uniqueMovies с количеством жанров = genreIdArray.length
                    const genresCount = genreIdArray.length
                    movies = uniqueMovies.filter(item => item.genresCount === genresCount).map(item1 => item1.movieId)

                    return await Movies.find({ _id: { $in: movies }, name: { $regex: name, $options: "i" } }).skip(skipCount).sort(sort).limit(PAGE_LIMIT)
                }
                return await Movies.find({ name: { $regex: name, $options: "i" } }).skip(skipCount).sort(sort).limit(PAGE_LIMIT)
            },
        },
        moviesPageCount: { 
            type: GraphQLInt,
            args: { genreId: { type: GraphQLString } },
            async resolve(parent, args, context) {
                if (!isAuthorized(context.headers)) {
                    return new Error('Unauthorized')
                }
                let moviesCount
                // Если задан genreId, отбираем все фильмы с этим жанром
                if (args.genreId) {
                    genreIdArray = args.genreId.split(',')
                    // Получаем фильмы по всем указанным в genreId жанрам
                    let mg = await MovieGenres.find({ genreId: { $in: genreIdArray } })
                    // Формируем массив уникальных movieId
                    const uniqueMovies = []
                    mg.forEach(item => {
                        if (!uniqueMovies.find(item1 => item.movieId === item1.movieId)) {
                            uniqueMovies.push({ movieId: item.movieId, genresCount: 0 })
                        }
                    })
                    // Считаем кол-во жанров для каждого фильма
                    mg.forEach(item => {
                        const moviePos = uniqueMovies.findIndex(item1 => item.movieId === item1.movieId)
                        uniqueMovies[moviePos].genresCount += 1
                    })
                    // Выбираем элементы uniqueMovies с количеством жанров === genreIdArray.length
                    const genresCount = genreIdArray.length
                    const movies = uniqueMovies.filter(item => item.genresCount === genresCount).map(item1 => item1.movieId)
                    moviesCount = await Movies.countDocuments({ _id: { $in: movies } })
                } else {
                    moviesCount = await Movies.countDocuments()
                }
                return Math.ceil(moviesCount / PAGE_LIMIT)
            }
        },
        directors: {
            type: new GraphQLList(DirectorType),
            args: { name: { type: GraphQLString }, page: { type: GraphQLInt } },
            async resolve(parent, { name = '', page = 0 }, context) {
                if (!isAuthorized(context.headers)) {
                    return new Error('Unauthorized')
                }
                let skipCount = 0
                if (page > 0) {
                    skipCount = PAGE_LIMIT * page
                }
                const directors = await Directors.find({ name: { $regex: name, $options: "i" } }).skip(skipCount).limit(PAGE_LIMIT)
                return directors
            },
        },
        directorsPageCount: {
            type: GraphQLInt,
            args: {},
            async resolve(parent, args, context) {
                if (!isAuthorized(context.headers)) {
                    return new Error('Unauthorized')
                }
                directorsCount = await Directors.countDocuments()
                return Math.ceil(directorsCount / PAGE_LIMIT)
            }
        },
        genres: {
            type: new GraphQLList(GenreType),
            args: { name: { type: GraphQLString }, genreId: { type: GraphQLString } },
            async resolve(parent, { name = '', genreId  }, context) {
                if (!isAuthorized(context.headers)) {
                    return new Error('Unauthorized')
                }
                if (genreId) {
                    genreIdArray = genreId.split(',')
                    return await Genres.find({ _id: { $in: genreIdArray } })
                }
                return await Genres.find({ name: { $regex: name, $options: "i" } })
            },
        },
        search: {
            type: SearchType,
            args: { searchString: { type: new GraphQLNonNull(GraphQLString) } },
            async resolve(parent, args, context) {
                if (!isAuthorized(context.headers)) {
                    return new Error('Unauthorized')
                }
                const movies = await Movies.find({ name: { $regex: args.searchString } })
                const directors = await Directors.find({ name: { $regex: args.searchString } })
                return { movies, directors }
            }
        },
        login: {
            type: LoginType,
            args: { login: { type: new GraphQLNonNull(GraphQLString) }, pwd: { type: new GraphQLNonNull(GraphQLString) } },
            async resolve(paremt, args) {
                const user = await Users.findOne({ login: args.login }, 'login pwd').exec()
                if (!user || user.pwd !== args.pwd) return { login: '', token: '' }
                const token = jwt.sign({ login: args.login }, SECRET, { expiresIn: "2d" })
                return { login: args.login, token }
            }
        },
        userByToken: {
            type: GraphQLString,
            args: { token: { type: new GraphQLNonNull(GraphQLString) } },
            async resolve(parent, args, context) {
                if (!isAuthorized(context.headers)) {
                    return new Error('Unauthorized')
                }
                const user = jwt.verify(args.token, SECRET)
                return user.login
            }
        }
    },
})

module.exports = new GraphQLSchema({
    query: Query,
    mutation: Mutation,
})
